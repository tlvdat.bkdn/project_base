<?php

use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Route;

Route::group(["middleware" => "guest:admin-web", "as" => "admin.", "prefix" => "/admin"], function () {
    Route::get('/login', [AdminController::class, 'login'])
    ->name('register');

    Route::post('login', [AdminController::class, 'store'])->name('loginAdmin');
});

Route::group(["middleware" => "auth:admin-web", "as" => "admin.", "prefix" => "/admin"], function () {
    Route::get('/dashboard', [AdminController::class, 'dashboard'])
    ->name('dashboard');
    Route::post('logout', [AdminController::class, 'destroy'])
        ->name('logout');
});
