clone project
- git clone https://gitlab.com/tlvdat.bkdn/project_base.git
clone laradock
- git clone https://github.com/Laradock/laradock.git

<p>Laravel Framework 10.25.2 </p>
<p>PHP 8.2</p>
<p>mysql 8.0</p>
<p>nginx version: nginx/1.25.2</p>

- composer install

- npm install

- npm run build

- edit .env
    <p>GOOGLE_CLIENT_ID=</p>
    <p>GOOGLE_CLIENT_SECRET=</p>
    <p>GOOGLE_REDIRECT=</p>
    <p>(Register at links https://console.cloud.google.com/)</p>

- art migrate --seed

<p>url user : http://localhost/login</p> 
<p>user account : user1@gmail.com</p> 
<p>user pass : 12345678</p> 


<p>url admin : http://localhost/admin/login</p>
<p>admin account : admin@gmail.com</p> 
<p>admin pass : 12345678</p> 

- use forgot password
    <p>MAIL_MAILER=</p>
    <p>MAIL_HOST=</p>
    <p>MAIL_PORT=</p>
    <p>MAIL_USERNAME=</p>
    <p>MAIL_PASSWORD=</p>
    <p>(Register at links https://mailtrap.io/)</p>
