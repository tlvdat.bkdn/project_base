<?php

namespace App\Services;

use App\Repositories\UserRepository;
use Illuminate\Support\Facades\DB;
use Exception;

/**
 * Interface UserRepository.
 *
 * @package namespace App\Repositories;
 */
class UserService
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function saveProfileUser($dataUserTable, $dataUserInfoTable, $idUser)
    {
        try {
            DB::beginTransaction();
            $user = $this->userRepository->update($dataUserTable, $idUser);
            $userInfo = $user->userInfo()->updateOrCreate(['user_id' => $idUser], $dataUserInfoTable);

            DB::commit();

            return true;
        } catch (Exception $e) {
            DB::rollBack();
            logger($e->getMessage());
            return false;
        }

    }
}
