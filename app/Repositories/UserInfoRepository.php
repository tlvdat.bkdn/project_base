<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserInfoRepository.
 *
 * @package namespace App\Repositories;
 */
interface UserInfoRepository extends RepositoryInterface
{
    //
}
