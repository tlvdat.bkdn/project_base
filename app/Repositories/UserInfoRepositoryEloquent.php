<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\UserInfoRepository;
use App\Entities\UserInfo;
use App\Validators\UserInfoValidator;

/**
 * Class UserInfoRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class UserInfoRepositoryEloquent extends BaseRepository implements UserInfoRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserInfo::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
