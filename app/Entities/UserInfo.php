<?php

namespace App\Entities;

use App\Models\City;
use App\Models\Country;
use App\Models\State;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class UserInfo.
 *
 * @package namespace App\Entities;
 */
class UserInfo extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'phone',
        'country_id',
        'state_id',
        'city_id',
        'address',
        'postcode',
        'des_self',
    ];
    protected $table = 'users_info';

    public function country()
    {
        return $this->belongsTo(Country::class)->select(['id', 'name']);
    }

    public function state()
    {
        return $this->belongsTo(State::class)->select(['id', 'name']);
    }

    public function city()
    {
        return $this->belongsTo(City::class)->select(['id', 'name']);
    }

}
