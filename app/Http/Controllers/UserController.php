<?php

namespace App\Http\Controllers;

use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function dashboard()
    {
        return view('user.dashboard');
    }

    public function viewSetting()
    {
        return view('user.setting');
    }

    public function viewProfile()
    {
        $user = Auth::guard('web')->user();
        return view('user.profile', compact('user'));
    }

    public function storeProfile(Request $request, $id)
    {
        $dataUserTable = $request->only('name', 'email');
        $dataUserInfoTable = $request->only('phone', 'country_id', 'state_id', 'city_id', 'address', 'postcode', 'des_self');

        $result = $this->userService->saveProfileUser($dataUserTable, $dataUserInfoTable, $id);

        if($result) {
            return redirect()->route('user.viewProfile', $id)->with('success', __('Successfully!'));
        }else {
            return redirect()->back()->with('error', __('Faild!'));
        }
    }
}
