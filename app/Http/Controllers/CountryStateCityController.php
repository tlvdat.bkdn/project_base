<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Country;
use App\Models\State;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CountryStateCityController extends Controller
{
    public function apiCountry()
    {
        return response()->json(Country::all(), Response::HTTP_OK);
    }

    public function apiState(Request $request)
    {
        return response()->json(State::where('country_id', $request->get('country_id'))->get(), Response::HTTP_OK);
    }

    public function apiCity(Request $request)
    {
        return response()->json(City::where('state_id', $request->get('state_id'))->get(), Response::HTTP_OK);
    }
}
