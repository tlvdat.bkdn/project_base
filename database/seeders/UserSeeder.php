<?php

namespace Database\Seeders;

use App\Entities\Admin;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::create([
            "name" => "User 1",
            "email" => "user1@gmail.com",
            "password" => Hash::make("12345678")
        ]);

        Admin::create([
            "name" => "Admin",
            "email" => "admin@gmail.com",
            "password" => Hash::make("12345678")
        ]);
    }
}
