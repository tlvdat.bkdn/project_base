<div class="modal bottom side fade" id="Modalstory" tabindex="-1" style="overflow-y: auto; display: none;"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content border-0 bg-transparent">
            <button type="button" class="close mt-0 position-absolute top--30 right--10" data-dismiss="modal"
                aria-label="Close"><i class="ti-close text-grey-900 font-xssss"></i></button>
            <div class="modal-body p-0">
                <div class="card w-100 border-0 rounded-3 overflow-hidden bg-gradiant-bottom bg-gradiant-top">
                    <div
                        class="owl-carousel owl-theme dot-style3 story-slider owl-dot-nav nav-none owl-loaded owl-drag owl-hidden">
                        <div class="owl-stage-outer">
                            <div class="owl-stage"
                                style="transform: translate3d(-380px, 0px, 0px); transition: all 0s ease 0s; width: 3040px;">
                                <div class="owl-item cloned" style="width: 380px;">
                                    <div class="item"><img src="/assets/user-resource/images/story-7.jpg"
                                            alt="image"></div>
                                </div>
                                <div class="owl-item cloned" style="width: 380px;">
                                    <div class="item"><img src="/assets/user-resource/images/story-8.jpg"
                                            alt="image"></div>
                                </div>
                                <div class="owl-item active" style="width: 380px;">
                                    <div class="item"><img src="/assets/user-resource/images/story-5.jpg"
                                            alt="image"></div>
                                </div>
                                <div class="owl-item" style="width: 380px;">
                                    <div class="item"><img src="/assets/user-resource/images/story-6.jpg"
                                            alt="image"></div>
                                </div>
                                <div class="owl-item" style="width: 380px;">
                                    <div class="item"><img src="/assets/user-resource/images/story-7.jpg"
                                            alt="image"></div>
                                </div>
                                <div class="owl-item" style="width: 380px;">
                                    <div class="item"><img src="/assets/user-resource/images/story-8.jpg"
                                            alt="image"></div>
                                </div>
                                <div class="owl-item cloned" style="width: 380px;">
                                    <div class="item"><img src="/assets/user-resource/images/story-5.jpg"
                                            alt="image"></div>
                                </div>
                                <div class="owl-item cloned" style="width: 380px;">
                                    <div class="item"><img src="/assets/user-resource/images/story-6.jpg"
                                            alt="image"></div>
                                </div>
                            </div>
                        </div>
                        <div class="owl-nav"><button type="button" role="presentation" class="owl-prev"><i
                                    class="ti-angle-left icon-nav"></i></button><button type="button"
                                role="presentation" class="owl-next"><i class="ti-angle-right icon-nav"></i></button>
                        </div>
                        <div class="owl-dots"><button role="button" class="owl-dot"><span></span></button><button
                                role="button" class="owl-dot active"><span></span></button><button role="button"
                                class="owl-dot"><span></span></button><button role="button"
                                class="owl-dot"><span></span></button></div>
                    </div>
                </div>
                <div class="form-group mt-3 mb-0 p-3 position-absolute bottom-0 z-index-1 w-100">
                    <input type="text"
                        class="style2-input w-100 bg-transparent border-light-md p-3 pe-5 font-xssss fw-500 text-white"
                        value="Write Comments">
                    <span class="feather-send text-white font-md text-white position-absolute"
                        style="bottom: 35px;right:30px;"></span>
                </div>
            </div>
        </div>
    </div>
</div>
