<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="{{ asset('assets/user-resource/css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/user-resource/css/feather.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/user-resource/css/style.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/emoji-picker/1.3.0/js/emoji-picker.min.js">
    <link rel="stylesheet" href="{{ asset('assets/user-resource/css/lightbox.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/user-resource/vendor/owl-carousel/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/user-resource/vendor/owl-carousel/css/owl.theme.default.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/user-resource/css/style-custom.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/user-resource/css/toast.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/select2/select2.min.css') }}">
    @yield('css')
</head>

<body class="color-theme-blue mont-font loaded">
    <div class="preloader" style="display: none;"></div>
    <div class="main-wrapper">

        @include('layouts.user.navigation')

        @include('layouts.user.navabar-left')

        @yield('content')

        @include('layouts.user.rightchat')

        @include('layouts.user.footer')

        @include('layouts.user.search-header')
    </div>

    @include('layouts.user.modal-story')

    @include('layouts.user.popupchat')

    <div id="lightboxOverlay" tabindex="-1" class="lightboxOverlay" style="display: none;"></div>
    <div id="lightbox" tabindex="-1" class="lightbox" style="display: none;">
        <div class="lb-outerContainer">
            <div class="lb-container"><img class="lb-image"
                    src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="
                    alt="">
                <div class="lb-nav"><a class="lb-prev" aria-label="Previous image" href=""></a><a class="lb-next"
                        aria-label="Next image" href=""></a></div>
                <div class="lb-loader"><a class="lb-cancel"></a></div>
            </div>
        </div>
        <div class="lb-dataContainer">
            <div class="lb-data">
                <div class="lb-details"><span class="lb-caption"></span><span class="lb-number"></span></div>
                <div class="lb-closeContainer"><a class="lb-close"></a></div>
            </div>
        </div>
        <div class="right-comment chat-left scroll-bar theme-dark-bg">
            <div class="card-body ps-2 pe-4 pb-0 d-flex">
                <figure class="avatar me-3"><img src="/assets/user-resource/images/user-8.png" alt="image"
                        class="shadow-sm rounded-circle w45"></figure>
                <h4 class="fw-700 text-grey-900 font-xssss mt-1 text-left">Hurin Seary <span
                        class="d-block font-xssss fw-500 mt-1 lh-3 text-grey-500">2 hour ago</span></h4> <a
                    href="#" class="ms-auto"><i
                        class="ti-more-alt text-grey-900 btn-round-md bg-greylight font-xss"></i></a>
            </div>
            <div class="card-body d-flex ps-2 pe-4 pt-0 mt-0"> <a href="#"
                    class="d-flex align-items-center fw-600 text-grey-900 lh-26 font-xssss me-3 text-dark"><i
                        class="feather-thumbs-up text-white bg-primary-gradiant me-1 btn-round-xs font-xss"></i> <i
                        class="feather-heart text-white bg-red-gradiant me-2 btn-round-xs font-xss"></i>2.8K Like</a>
                <a href="#" class="d-flex align-items-center fw-600 text-grey-900 lh-26 font-xssss text-dark"><i
                        class="feather-message-circle text-grey-900 btn-round-sm font-lg text-dark"></i>22 Comment</a>
            </div>
            <div class="card w-100 border-0 shadow-none right-scroll-bar">
                <div class="card-body border-top-xs pt-4 pb-3 pe-4 d-block ps-5">
                    <figure class="avatar position-absolute left-0 ms-2 mt-1"><img
                            src="/assets/user-resource/images/user-6.png" alt="image"
                            class="shadow-sm rounded-circle w35"></figure>
                    <div class="chat p-3 bg-greylight rounded-xxl d-block text-left theme-dark-bg">
                        <h4 class="fw-700 text-grey-900 font-xssss mt-0 mb-1">Victor Exrixon <a href="#"
                                class="ms-auto"><i class="ti-more-alt float-right text-grey-800 font-xsss"></i></a>
                        </h4>
                        <p class="fw-500 text-grey-500 lh-20 font-xssss w-100 mt-2 mb-0">Lorem ipsum dolor sit amet,
                            consectetur adipiscing elit. Morbi nulla dolor.</p>
                    </div>
                </div>
                <div class="card-body pt-0 pb-3 pe-4 d-block ps-5">
                    <figure class="avatar position-absolute left-0 ms-2 mt-1"><img
                            src="/assets/user-resource/images/user-4.png" alt="image"
                            class="shadow-sm rounded-circle w35"></figure>
                    <div class="chat p-3 bg-greylight rounded-xxl d-block text-left theme-dark-bg">
                        <h4 class="fw-700 text-grey-900 font-xssss mt-0 mb-1">Surfiya Zakir <a href="#"
                                class="ms-auto"><i class="ti-more-alt float-right text-grey-800 font-xsss"></i></a>
                        </h4>
                        <p class="fw-500 text-grey-500 lh-20 font-xssss w-100 mt-2 mb-0">Lorem ipsum dolor sit amet,
                            consectetur adipiscing elit. Morbi nulla dolor.</p>
                    </div>
                </div>
                <div class="card-body pt-0 pb-3 pe-4 d-block ps-5 ms-5 position-relative">
                    <figure class="avatar position-absolute left-0 ms-2 mt-1"><img
                            src="/assets/user-resource/images/user-3.png" alt="image"
                            class="shadow-sm rounded-circle w35"></figure>
                    <div class="chat p-3 bg-greylight rounded-xxl d-block text-left theme-dark-bg">
                        <h4 class="fw-700 text-grey-900 font-xssss mt-0 mb-1">Goria Coast <a href="#"
                                class="ms-auto"><i class="ti-more-alt float-right text-grey-800 font-xsss"></i></a>
                        </h4>
                        <p class="fw-500 text-grey-500 lh-20 font-xssss w-100 mt-2 mb-0">Lorem ipsum dolor sit amet,
                            consectetur adipiscing elit.</p>
                    </div>
                </div>
                <div class="card-body pt-0 pb-3 pe-4 d-block ps-5 ms-5 position-relative">
                    <figure class="avatar position-absolute left-0 ms-2 mt-1"><img
                            src="/assets/user-resource/images/user-3.png" alt="image"
                            class="shadow-sm rounded-circle w35"></figure>
                    <div class="chat p-3 bg-greylight rounded-xxl d-block text-left theme-dark-bg">
                        <h4 class="fw-700 text-grey-900 font-xssss mt-0 mb-1">Hurin Seary <a href="#"
                                class="ms-auto"><i class="ti-more-alt float-right text-grey-800 font-xsss"></i></a>
                        </h4>
                        <p class="fw-500 text-grey-500 lh-20 font-xssss w-100 mt-2 mb-0">Lorem ipsum dolor sit amet,
                            consectetur adipiscing elit.</p>
                    </div>
                </div>
                <div class="card-body pt-0 pb-3 pe-4 d-block ps-5 ms-5 position-relative">
                    <figure class="avatar position-absolute left-0 ms-2 mt-1"><img
                            src="/assets/user-resource/images/user-3.png" alt="image"
                            class="shadow-sm rounded-circle w35"></figure>
                    <div class="chat p-3 bg-greylight rounded-xxl d-block text-left theme-dark-bg">
                        <h4 class="fw-700 text-grey-900 font-xssss mt-0 mb-1">David Goria <a href="#"
                                class="ms-auto"><i class="ti-more-alt float-right text-grey-800 font-xsss"></i></a>
                        </h4>
                        <p class="fw-500 text-grey-500 lh-20 font-xssss w-100 mt-2 mb-0">Lorem ipsum dolor sit amet,
                            consectetur adipiscing elit.</p>
                    </div>
                </div>
                <div class="card-body pt-0 pb-3 pe-4 d-block ps-5">
                    <figure class="avatar position-absolute left-0 ms-2 mt-1"><img
                            src="/assets/user-resource/images/user-4.png" alt="image"
                            class="shadow-sm rounded-circle w35"></figure>
                    <div class="chat p-3 bg-greylight rounded-xxl d-block text-left theme-dark-bg">
                        <h4 class="fw-700 text-grey-900 font-xssss mt-0 mb-1">Seary Victor <a href="#"
                                class="ms-auto"><i class="ti-more-alt float-right text-grey-800 font-xsss"></i></a>
                        </h4>
                        <p class="fw-500 text-grey-500 lh-20 font-xssss w-100 mt-2 mb-0">Lorem ipsum dolor sit amet,
                            consectetur adipiscing elit. Morbi nulla dolor.</p>
                    </div>
                </div>
                <div class="card-body pt-0 pb-3 pe-4 d-block ps-5">
                    <figure class="avatar position-absolute left-0 ms-2 mt-1"><img
                            src="/assets/user-resource/images/user-4.png" alt="image"
                            class="shadow-sm rounded-circle w35"></figure>
                    <div class="chat p-3 bg-greylight rounded-xxl d-block text-left theme-dark-bg">
                        <h4 class="fw-700 text-grey-900 font-xssss mt-0 mb-1">Ana Seary <a href="#"
                                class="ms-auto"><i class="ti-more-alt float-right text-grey-800 font-xsss"></i></a>
                        </h4>
                        <p class="fw-500 text-grey-500 lh-20 font-xssss w-100 mt-2 mb-0">Lorem ipsum dolor sit amet,
                            consectetur adipiscing elit. Morbi nulla dolor.</p>
                    </div>
                </div>
                <div class="card-body pt-0 pb-3 pe-4 d-block ps-5">
                    <figure class="avatar position-absolute left-0 ms-2 mt-1"><img
                            src="/assets/user-resource/images/user-4.png" alt="image"
                            class="shadow-sm rounded-circle w35"></figure>
                    <div class="chat p-3 bg-greylight rounded-xxl d-block text-left theme-dark-bg">
                        <h4 class="fw-700 text-grey-900 font-xssss mt-0 mb-1">Studio Express <a href="#"
                                class="ms-auto"><i class="ti-more-alt float-right text-grey-800 font-xsss"></i></a>
                        </h4>
                        <p class="fw-500 text-grey-500 lh-20 font-xssss w-100 mt-2 mb-0">Lorem ipsum dolor sit amet,
                            consectetur adipiscing elit. Morbi nulla dolor.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="loom-companion-mv3" ext-id="liecbddmkiiihnedobmlmillhodjkdmb">
        <section id="shadow-host-companion"></section>
    </div>
    <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/user-resource/js/scripts.js') }}"></script>
    <script src="{{ asset('assets/user-resource/js/plugin.js') }}"></script>
    <script src="{{ asset('assets/user-resource/js/lightbox.js') }}"></script>
    <script src="{{ asset('assets/user-resource/js/toast.min.js') }}"></script>
    <script src="{{ asset('assets/user-resource/js/helper.js') }}"></script>
    <script src="{{ asset('assets/vendor/select2/select2.min.js') }}"></script>
    <script>
        $(function() {
           const msgSuccess = @json(session()->get('success'));
           const msgError = @json(session()->get('error'));
           msgSuccess && helper.method_helper.setToast('Success', msgSuccess, 'success');
           msgError && helper.method_helper.setToast('Error', msgError, 'error');
        });
    </script>
    @yield('script')
</body>

</html>
