<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="{{ asset('assets/vendor/css/core.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/fonts/boxicons.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/css/theme-default.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/admin/demo.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/libs/apex-charts/apex-charts.css') }}">
    @yield('css')
</head>

<body>
    <!-- Layout wrapper -->
    <div class="layout-wrapper layout-content-navbar">
        <div class="layout-container">
            @include('layouts.admin.sidebar')

            <!-- Layout container -->
            <div class="layout-page">
                @include('layouts.admin.navbar')

                <!-- Content wrapper -->
                <div class="content-wrapper">
                    @yield('content')

                    @include('layouts.admin.footer')

                    <div class="content-backdrop fade"></div>
                </div>
                <!-- Content wrapper -->
            </div>
            <!-- / Layout page -->
        </div>

        <!-- Overlay -->
        <div class="layout-overlay layout-menu-toggle"></div>
    </div>
    <!-- / Layout wrapper -->

    <!-- Place this tag in your head or just before your close body tag. -->
    <script async="" defer="" src="https://buttons.github.io/buttons.js"></script>


    <svg id="SvgjsSvg1283" width="2" height="0" xmlns="http://www.w3.org/2000/svg" version="1.1"
        xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.dev"
        style="overflow: hidden; top: -100%; left: -100%; position: absolute; opacity: 0;">
        <defs id="SvgjsDefs1284"></defs>
        <polyline id="SvgjsPolyline1285" points="0,0"></polyline>
        <path id="SvgjsPath1286"
            d="M-1 217.1625L-1 217.1625C-1 217.1625 41.38392857142858 217.1625 41.38392857142858 217.1625C41.38392857142858 217.1625 82.76785714285715 217.1625 82.76785714285715 217.1625C82.76785714285715 217.1625 124.15178571428572 217.1625 124.15178571428572 217.1625C124.15178571428572 217.1625 165.5357142857143 217.1625 165.5357142857143 217.1625C165.5357142857143 217.1625 206.91964285714286 217.1625 206.91964285714286 217.1625C206.91964285714286 217.1625 248.30357142857144 217.1625 248.30357142857144 217.1625C248.30357142857144 217.1625 289.6875 217.1625 289.6875 217.1625C289.6875 217.1625 289.6875 217.1625 289.6875 217.1625 ">
        </path>
    </svg>

    <script src="{{ asset('assets/vendor/js/helpers.js') }}"></script>
    <script src="{{ asset('assets/js/admin/config.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/libs/popper/popper.js') }}"></script>
    <script src="{{ asset('assets/vendor/js/bootstrap.js') }}"></script>
    <script src="{{ asset('assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js') }}"></script>
    <script src="{{ asset('assets/vendor/js/menu.js') }}"></script>
    <script src="{{ asset('assets/vendor/libs/apex-charts/apexcharts.js') }}"></script>
    <script src="{{ asset('assets/js/admin/main.js') }}"></script>
    <script src="{{ asset('assets/js/admin/dashboards-analytics.js') }}"></script>
    <script src="{{ asset('butt') }}"></script>
    @yield('script')
</body>

</html>
