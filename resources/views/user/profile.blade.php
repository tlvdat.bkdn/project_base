@extends('layouts.user.app')

@section('title', $user->name)

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/user-resource/css/profile.css') }}">
@endsection

@section('script')
    <script>
        const URL_API_COUNTRY = @json(route('apiCountry'));
        const URL_API_STATE = @json(route('apiState'));
        const URL_API_CITY = @json(route('apiCity'));
        const DATA_COUTRY = {};
        DATA_COUTRY.country = @json($user?->userInfo?->country);
        DATA_COUTRY.state = @json($user->userInfo?->state);
        DATA_COUTRY.city = @json($user?->userInfo?->city);
    </script>
    <script src="{{ asset('assets/user-resource/js/profile.js') }}"></script>
@endsection

@section('content')
    <div class="main-content bg-lightblue theme-dark-bg right-chat-active">

        <div class="middle-sidebar-bottom">
            <div class="middle-sidebar-left">
                <div class="middle-wrap">
                    <div class="card w-100 border-0 bg-white shadow-xs p-0 mb-4">
                        <div class="card-body p-4 w-100 bg-current border-0 d-flex rounded-3">
                            <a href="{{ route('user.viewSetting') }}" class="d-inline-block mt-2"><i
                                    class="ti-arrow-left font-sm text-white"></i></a>
                            <h4 class="font-xs text-white fw-600 ms-4 mb-0 mt-2">Account Details</h4>
                        </div>
                        <div class="card-body p-lg-5 p-4 w-100 border-0 ">
                            <div class="row justify-content-center">
                                <div class="col-lg-4 text-center">
                                    <figure class="avatar ms-auto me-auto mb-0 mt-2 w100"><img src="{{ $user->avatar }}"
                                            alt="image" class="shadow-sm rounded-3 w-100"></figure>
                                    <h2 class="fw-700 font-sm text-grey-900 mt-3">{{ $user->name }}</h2>
                                    <h4 class="text-grey-500 fw-500 mb-3 font-xsss mb-4">{{__('Chua biet dien gi')}}</h4>
                                    <!-- <a href="#" class="p-3 alert-primary text-primary font-xsss fw-500 mt-2 rounded-3">Upload New Photo</a> -->
                                </div>
                            </div>

                            <form action="{{ route('user.storeProfile', $user->id) }}" method="POST">
                                @csrf
                                <div class="row">
                                    <div class="col-lg-12 mb-3">
                                        <div class="form-group">
                                            <label class="mont-font fw-600 font-xsss">Name</label>
                                            <input type="text" class="form-control" name="name" value="{{ old('name', $user->name) }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6 mb-3">
                                        <div class="form-group">
                                            <label class="mont-font fw-600 font-xsss">Email</label>
                                            <input type="text" class="form-control" name="email" value="{{ old('email', $user->email)}}">
                                        </div>
                                    </div>

                                    <div class="col-lg-6 mb-3">
                                        <div class="form-group">
                                            <label class="mont-font fw-600 font-xsss">Phone</label>
                                            <input type="text" class="form-control" value="{{ old('phone', $user?->userInfo?->phone) }}" name="phone">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12 mb-3">
                                        <div class="form-group">
                                            <label class="mont-font fw-600 font-xsss">Country</label>
                                            <select type="text" class="form-control" name="country_id" id="country_id">
                                                <option value=""></option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 mb-3">
                                        <div class="form-group">
                                            <label class="mont-font fw-600 font-xsss">State</label>
                                            <select type="text" class="form-control" name="state_id" id="state_id">
                                                <option value=""></option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 mb-3">
                                        <div class="form-group">
                                            <label class="mont-font fw-600 font-xsss">Twon / City</label>
                                            <select type="text" class="form-control" name="city_id" id="city_id">
                                                <option value=""></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12 mb-3">
                                        <div class="form-group">
                                            <label class="mont-font fw-600 font-xsss">Address</label>
                                            <input type="text" class="form-control" value="{{ old('address', $user?->userInfo?->address) }}" name="address">
                                        </div>
                                    </div>

                                    <div class="col-lg-6 mb-3">
                                        <div class="form-group">
                                            <label class="mont-font fw-600 font-xsss">Postcode</label>
                                            <input type="text" class="form-control" value="{{ old('postcode', $user?->userInfo?->postcode) }}" name="postcode">
                                        </div>
                                    </div>

                                    <div class="col-lg-12 mb-3">
                                        <div class="card mt-3 border-0">
                                            <div class="card-body d-flex justify-content-between align-items-end p-0">
                                                <div class="form-group mb-0 w-100">
                                                    <input type="file" name="file" id="file" class="input-file">
                                                    <label for="file"
                                                        class="rounded-3 text-center bg-white btn-tertiary js-labelFile p-4 w-100 border-dashed">
                                                        <i class="ti-cloud-down large-icon me-3 d-block"></i>
                                                        <span class="js-fileName">Drag and drop or click to replace</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-12 mb-3">
                                        <label class="mont-font fw-600 font-xsss">Description</label>
                                        <textarea class="form-control mb-0 p-3 h100 bg-greylight lh-16" rows="5" placeholder="Write your message..." name="des_self"
                                            spellcheck="false">{{ old("des_self", $user?->userInfo?->des_self) }}</textarea>
                                    </div>

                                    <div class="col-lg-12">
                                        <button class="bg-current text-center text-white font-xsss fw-600 p-3 w175 rounded-3 d-inline-block">Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- <div class="card w-100 border-0 p-2"></div> -->
                </div>
            </div>

        </div>
    </div>
@endsection
