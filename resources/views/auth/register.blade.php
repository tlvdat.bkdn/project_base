@extends('layouts.guest')
@section('title', 'Register')
@section('css')
    <link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/font-awesome.css.map') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
@endsection

@section('content')
    <div class="img js-fullheight login">
        <section class="ftco-section">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-6 text-center mb-5">
                        <h2 class="heading-section">Register to MXH</h2>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-6 col-lg-4">
                        <div class="login-wrap p-0">
                            <h3 class="mb-4 text-center">Create an account!</h3>
                            <form method="POST" action="{{ route('register') }}" class="signin-form">
                                @csrf
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Name" required="" name="name" value="{{ old('name') }}" required>
                                @error('name')
                                    <h6 class="text-danger small">{{ $message }} <i class="fa fa-exclamation-circle" aria-hidden="true"></i></h6>
                                @enderror
                                </div>

                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Email" required="" name="email" value="{{ old('email') }}" required>
                                @error('email')
                                    <h6 class="text-danger small">{{ $message }} <i class="fa fa-exclamation-circle" aria-hidden="true"></i></h6>
                                @enderror
                                </div>

                                <div class="form-group">
                                    <input type="password" class="form-control" placeholder="Password" required="" name="password" required>
                                @error('password')
                                    <h6 class="text-danger small">{{ $message }} <i class="fa fa-exclamation-circle" aria-hidden="true"></i></h6>
                                @enderror
                                </div>

                                <div class="form-group">
                                    <input type="password" class="form-control" placeholder="Confirm Password" required="" name="password_confirmation" required>
                                @error('password_confirmation')
                                    <h6 class="text-danger small">{{ $message }} <i class="fa fa-exclamation-circle" aria-hidden="true"></i></h6>
                                @enderror
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="form-control btn btn-primary submit px-3">Register</button>
                                </div>
                            </form>

                            <p class="w-100 text-center">— Or Sign In With —</p>

                            <div class="social d-flex text-center justify-center">
                                <a href="{{ route('login.provider', 'google') }}" class="btn-login-google">
                                    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />

                                    <div class="google-btn">
                                      <div class="google-icon-wrapper">
                                        <img class="google-icon" src="https://upload.wikimedia.org/wikipedia/commons/5/53/Google_%22G%22_Logo.svg"/>
                                      </div>
                                      <p class="btn-text"><b>Sign in with google</b></p>
                                    </div>
                                </a>
                            </div>
                            <div class="text-center text-primary">
                                <a class="underline text-sm text-white hover:text-gray-900 rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500" href="{{ route('login') }}">
                                    {{ __('Already registered?') }}
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <div id="loom-companion-mv3" ext-id="liecbddmkiiihnedobmlmillhodjkdmb">
            <section id="shadow-host-companion"></section>
        </div>
    </div>
@endsection

@section('script')

@endsection

