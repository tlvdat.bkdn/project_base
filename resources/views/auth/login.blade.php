@extends('layouts.guest')
@section('title', 'Login')
@section('css')
    <link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/font-awesome.css.map') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
@endsection

@section('content')
    <div class="img js-fullheight login">
        <section class="ftco-section">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-6 text-center mb-5">
                        <h2 class="heading-section">Login to MXH</h2>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-6 col-lg-4">
                        <div class="login-wrap p-0">
                            <h3 class="mb-4 text-center">Have an account? If not yet can <a href="{{ route('register') }}">Register</a></h3>
                            <form method="POST" action="{{ route('login') }}" class="signin-form">
                                @csrf
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Email" required="" name="email" value="{{ old('email') }}" required>
                                    @error('email')
                                    <h6 class="text-danger small">{{ $message }} <i class="fa fa-exclamation-circle" aria-hidden="true"></i></h6>
                                @enderror
                                </div>

                                <div class="form-group">
                                    <input id="password-field" type="password" class="form-control" placeholder="Password" name="password"
                                        required="">
                                    @error('password')
                                        <h6 class="text-danger">{{ $message }} <i class="fa fa-exclamation-circle" aria-hidden="true"></i></h6>
                                    @enderror
                                    <span toggle="#password-field"
                                        class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="form-control btn btn-primary submit px-3">Sign In</button>
                                </div>
                                <div class="form-group d-md-flex">
                                    <div class="w-50">
                                        <label class="checkbox-wrap checkbox-primary">Remember Me
                                            <input type="checkbox" checked="">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                    <div class="w-50 text-md-right">
                                        <a href="{{ route('password.request') }}" style="color: #fff">Forgot Password</a>
                                    </div>
                                </div>
                            </form>
                            <p class="w-100 text-center">— Or Sign In With —</p>
                            <div class="social d-flex text-center justify-center">
                                <a href="{{ route('login.provider', 'google') }}" class="btn-login-google">
                                    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />

                                    <div class="google-btn">
                                      <div class="google-icon-wrapper">
                                        <img class="google-icon" src="https://upload.wikimedia.org/wikipedia/commons/5/53/Google_%22G%22_Logo.svg"/>
                                      </div>
                                      <p class="btn-text"><b>Sign in with google</b></p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <div id="loom-companion-mv3" ext-id="liecbddmkiiihnedobmlmillhodjkdmb">
            <section id="shadow-host-companion"></section>
        </div>
    </div>
@endsection

@section('script')

@endsection
