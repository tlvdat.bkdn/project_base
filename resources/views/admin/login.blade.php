@extends('layouts.guest')
@section('title', 'Login')
@section('css')
    <link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/font-awesome.css.map') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
@endsection

@section('content')
    <div class="img js-fullheight login">
        <section class="ftco-section">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-6 text-center mb-5">
                        <h2 class="heading-section">Login to Admin MXH</h2>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-6 col-lg-4">
                        <div class="login-wrap p-0">
                            <form method="POST" action="{{ route('admin.loginAdmin') }}" class="signin-form">
                                @csrf
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Account" required="" name="email" value="{{ old('email') }}" required>
                                    @error('email')
                                    <h6 class="text-danger small">{{ $message }} <i class="fa fa-exclamation-circle" aria-hidden="true"></i></h6>
                                @enderror
                                </div>

                                <div class="form-group">
                                    <input id="password-field" type="password" class="form-control" placeholder="Password" name="password"
                                        required="">
                                    @error('password')
                                        <h6 class="text-danger">{{ $message }} <i class="fa fa-exclamation-circle" aria-hidden="true"></i></h6>
                                    @enderror
                                    <span toggle="#password-field"
                                        class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="form-control btn btn-primary submit px-3">Sign In</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <div id="loom-companion-mv3" ext-id="liecbddmkiiihnedobmlmillhodjkdmb">
            <section id="shadow-host-companion"></section>
        </div>
    </div>
@endsection

@section('script')

@endsection
