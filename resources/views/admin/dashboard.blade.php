@extends('layouts.admin.app')

@section('title', 'Dashboard')

@section('css')

@endsection

@section('script')

@endsection


@section('content')
    <!-- Content -->

    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="row">
            <div class="col-lg-8 mb-4 order-0">
                <div class="card">
                    <div class="d-flex align-items-end row">
                        <div class="col-sm-7">
                            <div class="card-body">
                                <h5 class="card-title text-primary">Congratulations John! 🎉</h5>
                                <p class="mb-4">
                                    You have done <span class="fw-medium">72%</span> more sales today.
                                    Check your new badge in
                                    your profile.
                                </p>

                                <a href="javascript:;" class="btn btn-sm btn-outline-primary">View
                                    Badges</a>
                            </div>
                        </div>
                        <div class="col-sm-5 text-center text-sm-left">
                            <div class="card-body pb-0 px-0 px-md-4">
                                <img src="../assets/img/illustrations/man-with-laptop-light.png"
                                    height="140" alt="View Badge User"
                                    data-app-dark-img="illustrations/man-with-laptop-dark.png"
                                    data-app-light-img="illustrations/man-with-laptop-light.png">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 order-1">
                <div class="row">
                    <div class="col-lg-6 col-md-12 col-6 mb-4">
                        <div class="card">
                            <div class="card-body">
                                <div
                                    class="card-title d-flex align-items-start justify-content-between">
                                    <div class="avatar flex-shrink-0">
                                        <img src="../assets/img/icons/unicons/chart-success.png"
                                            alt="chart success" class="rounded">
                                    </div>
                                    <div class="dropdown">
                                        <button class="btn p-0" type="button" id="cardOpt3"
                                            data-bs-toggle="dropdown" aria-haspopup="true"
                                            aria-expanded="false">
                                            <i class="bx bx-dots-vertical-rounded"></i>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-end"
                                            aria-labelledby="cardOpt3">
                                            <a class="dropdown-item" href="javascript:void(0);">View
                                                More</a>
                                            <a class="dropdown-item"
                                                href="javascript:void(0);">Delete</a>
                                        </div>
                                    </div>
                                </div>
                                <span class="fw-medium d-block mb-1">Profit</span>
                                <h3 class="card-title mb-2">$12,628</h3>
                                <small class="text-success fw-medium"><i class="bx bx-up-arrow-alt"></i>
                                    +72.80%</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-6 mb-4">
                        <div class="card">
                            <div class="card-body">
                                <div
                                    class="card-title d-flex align-items-start justify-content-between">
                                    <div class="avatar flex-shrink-0">
                                        <img src="../assets/img/icons/unicons/wallet-info.png"
                                            alt="Credit Card" class="rounded">
                                    </div>
                                    <div class="dropdown">
                                        <button class="btn p-0" type="button" id="cardOpt6"
                                            data-bs-toggle="dropdown" aria-haspopup="true"
                                            aria-expanded="false">
                                            <i class="bx bx-dots-vertical-rounded"></i>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-end"
                                            aria-labelledby="cardOpt6">
                                            <a class="dropdown-item" href="javascript:void(0);">View
                                                More</a>
                                            <a class="dropdown-item"
                                                href="javascript:void(0);">Delete</a>
                                        </div>
                                    </div>
                                </div>
                                <span>Sales</span>
                                <h3 class="card-title text-nowrap mb-1">$4,679</h3>
                                <small class="text-success fw-medium"><i class="bx bx-up-arrow-alt"></i>
                                    +28.42%</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Total Revenue -->
            <div class="col-12 col-lg-8 order-2 order-md-3 order-lg-2 mb-4">
                <div class="card">
                    <div class="row row-bordered g-0">
                        <div class="col-md-8">
                            <h5 class="card-header m-0 me-2 pb-3">Total Revenue</h5>
                            <div id="totalRevenueChart" class="px-2" style="min-height: 315px;">
                                <div id="apexchartst035o2mbi"
                                    class="apexcharts-canvas apexchartst035o2mbi apexcharts-theme-light"
                                    style="width: 411px; height: 300px;"><svg id="SvgjsSvg1001"
                                        width="411" height="300"
                                        xmlns="http://www.w3.org/2000/svg" version="1.1"
                                        xmlns:xlink="http://www.w3.org/1999/xlink"
                                        xmlns:svgjs="http://svgjs.dev" class="apexcharts-svg"
                                        xmlns:data="ApexChartsNS" transform="translate(0, 0)"
                                        style="background: transparent;">
                                        <foreignObject x="0" y="0" width="411"
                                            height="300">
                                            <div class="apexcharts-legend apexcharts-align-left apx-legend-position-top"
                                                xmlns="http://www.w3.org/1999/xhtml"
                                                style="right: 0px; position: absolute; left: 0px; top: 4px; max-height: 150px;">
                                                <div class="apexcharts-legend-series" rel="1"
                                                    seriesname="2021" data:collapsed="false"
                                                    style="margin: 2px 10px;"><span
                                                        class="apexcharts-legend-marker"
                                                        rel="1" data:collapsed="false"
                                                        style="background: rgb(105, 108, 255) !important; color: rgb(105, 108, 255); height: 8px; width: 8px; left: -3px; top: 0px; border-width: 0px; border-color: rgb(255, 255, 255); border-radius: 12px;"></span><span
                                                        class="apexcharts-legend-text" rel="1"
                                                        i="0" data:default-text="2021"
                                                        data:collapsed="false"
                                                        style="color: rgb(55, 61, 63); font-size: 12px; font-weight: 400; font-family: Helvetica, Arial, sans-serif;">2021</span>
                                                </div>
                                                <div class="apexcharts-legend-series" rel="2"
                                                    seriesname="2020" data:collapsed="false"
                                                    style="margin: 2px 10px;"><span
                                                        class="apexcharts-legend-marker"
                                                        rel="2" data:collapsed="false"
                                                        style="background: rgb(3, 195, 236) !important; color: rgb(3, 195, 236); height: 8px; width: 8px; left: -3px; top: 0px; border-width: 0px; border-color: rgb(255, 255, 255); border-radius: 12px;"></span><span
                                                        class="apexcharts-legend-text" rel="2"
                                                        i="1" data:default-text="2020"
                                                        data:collapsed="false"
                                                        style="color: rgb(55, 61, 63); font-size: 12px; font-weight: 400; font-family: Helvetica, Arial, sans-serif;">2020</span>
                                                </div>
                                            </div>
                                            <style type="text/css">
                                                .apexcharts-legend {
                                                    display: flex;
                                                    overflow: auto;
                                                    padding: 0 10px;
                                                }

                                                .apexcharts-legend.apx-legend-position-bottom,
                                                .apexcharts-legend.apx-legend-position-top {
                                                    flex-wrap: wrap
                                                }

                                                .apexcharts-legend.apx-legend-position-right,
                                                .apexcharts-legend.apx-legend-position-left {
                                                    flex-direction: column;
                                                    bottom: 0;
                                                }

                                                .apexcharts-legend.apx-legend-position-bottom.apexcharts-align-left,
                                                .apexcharts-legend.apx-legend-position-top.apexcharts-align-left,
                                                .apexcharts-legend.apx-legend-position-right,
                                                .apexcharts-legend.apx-legend-position-left {
                                                    justify-content: flex-start;
                                                }

                                                .apexcharts-legend.apx-legend-position-bottom.apexcharts-align-center,
                                                .apexcharts-legend.apx-legend-position-top.apexcharts-align-center {
                                                    justify-content: center;
                                                }

                                                .apexcharts-legend.apx-legend-position-bottom.apexcharts-align-right,
                                                .apexcharts-legend.apx-legend-position-top.apexcharts-align-right {
                                                    justify-content: flex-end;
                                                }

                                                .apexcharts-legend-series {
                                                    cursor: pointer;
                                                    line-height: normal;
                                                }

                                                .apexcharts-legend.apx-legend-position-bottom .apexcharts-legend-series,
                                                .apexcharts-legend.apx-legend-position-top .apexcharts-legend-series {
                                                    display: flex;
                                                    align-items: center;
                                                }

                                                .apexcharts-legend-text {
                                                    position: relative;
                                                    font-size: 14px;
                                                }

                                                .apexcharts-legend-text *,
                                                .apexcharts-legend-marker * {
                                                    pointer-events: none;
                                                }

                                                .apexcharts-legend-marker {
                                                    position: relative;
                                                    display: inline-block;
                                                    cursor: pointer;
                                                    margin-right: 3px;
                                                    border-style: solid;
                                                }

                                                .apexcharts-legend.apexcharts-align-right .apexcharts-legend-series,
                                                .apexcharts-legend.apexcharts-align-left .apexcharts-legend-series {
                                                    display: inline-block;
                                                }

                                                .apexcharts-legend-series.apexcharts-no-click {
                                                    cursor: auto;
                                                }

                                                .apexcharts-legend .apexcharts-hidden-zero-series,
                                                .apexcharts-legend .apexcharts-hidden-null-series {
                                                    display: none !important;
                                                }

                                                .apexcharts-inactive-legend {
                                                    opacity: 0.45;
                                                }
                                            </style>
                                        </foreignObject>
                                        <g id="SvgjsG1003"
                                            class="apexcharts-inner apexcharts-graphical"
                                            transform="translate(53.796875, 52)">
                                            <defs id="SvgjsDefs1002">
                                                <linearGradient id="SvgjsLinearGradient1007"
                                                    x1="0" y1="0" x2="0"
                                                    y2="1">
                                                    <stop id="SvgjsStop1008" stop-opacity="0.4"
                                                        stop-color="rgba(216,227,240,0.4)"
                                                        offset="0">
                                                    </stop>
                                                    <stop id="SvgjsStop1009" stop-opacity="0.5"
                                                        stop-color="rgba(190,209,230,0.5)"
                                                        offset="1">
                                                    </stop>
                                                    <stop id="SvgjsStop1010" stop-opacity="0.5"
                                                        stop-color="rgba(190,209,230,0.5)"
                                                        offset="1">
                                                    </stop>
                                                </linearGradient>
                                                <clipPath id="gridRectMaskt035o2mbi">
                                                    <rect id="SvgjsRect1012" width="347.203125"
                                                        height="222.73" x="-5" y="-3"
                                                        rx="0" ry="0" opacity="1"
                                                        stroke-width="0" stroke="none"
                                                        stroke-dasharray="0" fill="#fff"></rect>
                                                </clipPath>
                                                <clipPath id="forecastMaskt035o2mbi"></clipPath>
                                                <clipPath id="nonForecastMaskt035o2mbi"></clipPath>
                                                <clipPath id="gridRectMarkerMaskt035o2mbi">
                                                    <rect id="SvgjsRect1013" width="341.203125"
                                                        height="220.73" x="-2" y="-2"
                                                        rx="0" ry="0" opacity="1"
                                                        stroke-width="0" stroke="none"
                                                        stroke-dasharray="0" fill="#fff"></rect>
                                                </clipPath>
                                            </defs>
                                            <rect id="SvgjsRect1011" width="19.26875" height="216.73"
                                                x="0" y="0" rx="0"
                                                ry="0" opacity="1" stroke-width="0"
                                                stroke-dasharray="3"
                                                fill="url(#SvgjsLinearGradient1007)"
                                                class="apexcharts-xcrosshairs" y2="216.73"
                                                filter="none" fill-opacity="0.9"></rect>
                                            <g id="SvgjsG1033" class="apexcharts-xaxis"
                                                transform="translate(0, 0)">
                                                <g id="SvgjsG1034" class="apexcharts-xaxis-texts-g"
                                                    transform="translate(0, -4)"><text
                                                        id="SvgjsText1036"
                                                        font-family="Helvetica, Arial, sans-serif"
                                                        x="24.0859375" y="245.73"
                                                        text-anchor="middle" dominant-baseline="auto"
                                                        font-size="13px" font-weight="400"
                                                        fill="#373d3f"
                                                        class="apexcharts-text apexcharts-xaxis-label "
                                                        style="font-family: Helvetica, Arial, sans-serif;">
                                                        <tspan id="SvgjsTspan1037">Jan</tspan>
                                                        <title>Jan</title>
                                                    </text><text id="SvgjsText1039"
                                                        font-family="Helvetica, Arial, sans-serif"
                                                        x="72.2578125" y="245.73"
                                                        text-anchor="middle" dominant-baseline="auto"
                                                        font-size="13px" font-weight="400"
                                                        fill="#373d3f"
                                                        class="apexcharts-text apexcharts-xaxis-label "
                                                        style="font-family: Helvetica, Arial, sans-serif;">
                                                        <tspan id="SvgjsTspan1040">Feb</tspan>
                                                        <title>Feb</title>
                                                    </text><text id="SvgjsText1042"
                                                        font-family="Helvetica, Arial, sans-serif"
                                                        x="120.4296875" y="245.73"
                                                        text-anchor="middle" dominant-baseline="auto"
                                                        font-size="13px" font-weight="400"
                                                        fill="#373d3f"
                                                        class="apexcharts-text apexcharts-xaxis-label "
                                                        style="font-family: Helvetica, Arial, sans-serif;">
                                                        <tspan id="SvgjsTspan1043">Mar</tspan>
                                                        <title>Mar</title>
                                                    </text><text id="SvgjsText1045"
                                                        font-family="Helvetica, Arial, sans-serif"
                                                        x="168.6015625" y="245.73"
                                                        text-anchor="middle" dominant-baseline="auto"
                                                        font-size="13px" font-weight="400"
                                                        fill="#373d3f"
                                                        class="apexcharts-text apexcharts-xaxis-label "
                                                        style="font-family: Helvetica, Arial, sans-serif;">
                                                        <tspan id="SvgjsTspan1046">Apr</tspan>
                                                        <title>Apr</title>
                                                    </text><text id="SvgjsText1048"
                                                        font-family="Helvetica, Arial, sans-serif"
                                                        x="216.7734375" y="245.73"
                                                        text-anchor="middle" dominant-baseline="auto"
                                                        font-size="13px" font-weight="400"
                                                        fill="#373d3f"
                                                        class="apexcharts-text apexcharts-xaxis-label "
                                                        style="font-family: Helvetica, Arial, sans-serif;">
                                                        <tspan id="SvgjsTspan1049">May</tspan>
                                                        <title>May</title>
                                                    </text><text id="SvgjsText1051"
                                                        font-family="Helvetica, Arial, sans-serif"
                                                        x="264.9453125" y="245.73"
                                                        text-anchor="middle" dominant-baseline="auto"
                                                        font-size="13px" font-weight="400"
                                                        fill="#373d3f"
                                                        class="apexcharts-text apexcharts-xaxis-label "
                                                        style="font-family: Helvetica, Arial, sans-serif;">
                                                        <tspan id="SvgjsTspan1052">Jun</tspan>
                                                        <title>Jun</title>
                                                    </text><text id="SvgjsText1054"
                                                        font-family="Helvetica, Arial, sans-serif"
                                                        x="313.1171875" y="245.73"
                                                        text-anchor="middle" dominant-baseline="auto"
                                                        font-size="13px" font-weight="400"
                                                        fill="#373d3f"
                                                        class="apexcharts-text apexcharts-xaxis-label "
                                                        style="font-family: Helvetica, Arial, sans-serif;">
                                                        <tspan id="SvgjsTspan1055">Jul</tspan>
                                                        <title>Jul</title>
                                                    </text></g>
                                            </g>
                                            <g id="SvgjsG1070" class="apexcharts-grid">
                                                <g id="SvgjsG1071"
                                                    class="apexcharts-gridlines-horizontal">
                                                    <line id="SvgjsLine1073" x1="0"
                                                        y1="0" x2="337.203125" y2="0"
                                                        stroke="#eceef1" stroke-dasharray="0"
                                                        stroke-linecap="butt"
                                                        class="apexcharts-gridline">
                                                    </line>
                                                    <line id="SvgjsLine1074" x1="0"
                                                        y1="43.346" x2="337.203125" y2="43.346"
                                                        stroke="#eceef1" stroke-dasharray="0"
                                                        stroke-linecap="butt"
                                                        class="apexcharts-gridline">
                                                    </line>
                                                    <line id="SvgjsLine1075" x1="0"
                                                        y1="86.692" x2="337.203125" y2="86.692"
                                                        stroke="#eceef1" stroke-dasharray="0"
                                                        stroke-linecap="butt"
                                                        class="apexcharts-gridline">
                                                    </line>
                                                    <line id="SvgjsLine1076" x1="0"
                                                        y1="130.03799999999998" x2="337.203125"
                                                        y2="130.03799999999998" stroke="#eceef1"
                                                        stroke-dasharray="0" stroke-linecap="butt"
                                                        class="apexcharts-gridline"></line>
                                                    <line id="SvgjsLine1077" x1="0"
                                                        y1="173.384" x2="337.203125" y2="173.384"
                                                        stroke="#eceef1" stroke-dasharray="0"
                                                        stroke-linecap="butt"
                                                        class="apexcharts-gridline">
                                                    </line>
                                                    <line id="SvgjsLine1078" x1="0"
                                                        y1="216.73" x2="337.203125" y2="216.73"
                                                        stroke="#eceef1" stroke-dasharray="0"
                                                        stroke-linecap="butt"
                                                        class="apexcharts-gridline">
                                                    </line>
                                                </g>
                                                <g id="SvgjsG1072"
                                                    class="apexcharts-gridlines-vertical">
                                                </g>
                                                <line id="SvgjsLine1080" x1="0"
                                                    y1="216.73" x2="337.203125" y2="216.73"
                                                    stroke="transparent" stroke-dasharray="0"
                                                    stroke-linecap="butt"></line>
                                                <line id="SvgjsLine1079" x1="0"
                                                    y1="1" x2="0" y2="216.73"
                                                    stroke="transparent" stroke-dasharray="0"
                                                    stroke-linecap="butt"></line>
                                            </g>
                                            <g id="SvgjsG1014"
                                                class="apexcharts-bar-series apexcharts-plot-series">
                                                <g id="SvgjsG1015" class="apexcharts-series"
                                                    seriesName="2021" rel="1"
                                                    data:realIndex="0">
                                                    <path id="SvgjsPath1017"
                                                        d="M14.4515625 120.03800000000001L14.4515625 62.01520000000002C14.451562500000001 55.34853333333335 17.784895833333334 52.01520000000002 24.4515625 52.01520000000002L17.7203125 52.01520000000002C24.386979166666666 52.01520000000002 27.7203125 55.34853333333335 27.7203125 62.01520000000002L27.7203125 62.01520000000002L27.7203125 120.03800000000001C27.7203125 126.70466666666667 24.386979166666666 130.038 17.7203125 130.038C17.7203125 130.038 24.4515625 130.038 24.4515625 130.038C17.784895833333334 130.038 14.451562500000001 126.70466666666667 14.4515625 120.03800000000001C14.4515625 120.03800000000001 14.4515625 120.03800000000001 14.4515625 120.03800000000001 "
                                                        fill="rgba(105,108,255,0.85)" fill-opacity="1"
                                                        stroke="#ffffff" stroke-opacity="1"
                                                        stroke-linecap="round" stroke-width="6"
                                                        stroke-dasharray="0"
                                                        class="apexcharts-bar-area" index="0"
                                                        clip-path="url(#gridRectMaskt035o2mbi)"
                                                        pathTo="M 14.4515625 120.03800000000001L 14.4515625 62.01520000000002Q 14.4515625 52.01520000000002 24.4515625 52.01520000000002L 17.7203125 52.01520000000002Q 27.7203125 52.01520000000002 27.7203125 62.01520000000002L 27.7203125 62.01520000000002L 27.7203125 120.03800000000001Q 27.7203125 130.038 17.7203125 130.038L 24.4515625 130.038Q 14.4515625 130.038 14.4515625 120.03800000000001z"
                                                        pathFrom="M 14.4515625 120.03800000000001L 14.4515625 120.03800000000001L 27.7203125 120.03800000000001L 27.7203125 120.03800000000001L 27.7203125 120.03800000000001L 27.7203125 120.03800000000001L 27.7203125 120.03800000000001L 14.4515625 120.03800000000001"
                                                        cy="52.01520000000002" cx="59.6234375"
                                                        j="0" val="18"
                                                        barHeight="78.02279999999999"
                                                        barWidth="19.26875">
                                                    </path>
                                                    <path id="SvgjsPath1018"
                                                        d="M62.6234375 120.03800000000001L62.6234375 109.69580000000002C62.6234375 103.02913333333335 65.95677083333334 99.69580000000002 72.6234375 99.69580000000002L65.8921875 99.69580000000002C72.55885416666668 99.69580000000002 75.8921875 103.02913333333335 75.8921875 109.69580000000002L75.8921875 109.69580000000002L75.8921875 120.03800000000001C75.8921875 126.70466666666667 72.55885416666668 130.038 65.8921875 130.038C65.8921875 130.038 72.6234375 130.038 72.6234375 130.038C65.95677083333334 130.038 62.6234375 126.70466666666667 62.6234375 120.03800000000001C62.6234375 120.03800000000001 62.6234375 120.03800000000001 62.6234375 120.03800000000001 "
                                                        fill="rgba(105,108,255,0.85)" fill-opacity="1"
                                                        stroke="#ffffff" stroke-opacity="1"
                                                        stroke-linecap="round" stroke-width="6"
                                                        stroke-dasharray="0"
                                                        class="apexcharts-bar-area" index="0"
                                                        clip-path="url(#gridRectMaskt035o2mbi)"
                                                        pathTo="M 62.6234375 120.03800000000001L 62.6234375 109.69580000000002Q 62.6234375 99.69580000000002 72.6234375 99.69580000000002L 65.8921875 99.69580000000002Q 75.8921875 99.69580000000002 75.8921875 109.69580000000002L 75.8921875 109.69580000000002L 75.8921875 120.03800000000001Q 75.8921875 130.038 65.8921875 130.038L 72.6234375 130.038Q 62.6234375 130.038 62.6234375 120.03800000000001z"
                                                        pathFrom="M 62.6234375 120.03800000000001L 62.6234375 120.03800000000001L 75.8921875 120.03800000000001L 75.8921875 120.03800000000001L 75.8921875 120.03800000000001L 75.8921875 120.03800000000001L 75.8921875 120.03800000000001L 62.6234375 120.03800000000001"
                                                        cy="99.69580000000002" cx="107.7953125"
                                                        j="1" val="7"
                                                        barHeight="30.3422" barWidth="19.26875">
                                                    </path>
                                                    <path id="SvgjsPath1019"
                                                        d="M110.7953125 120.03800000000001L110.7953125 75.01900000000002C110.7953125 68.35233333333335 114.12864583333334 65.01900000000002 120.7953125 65.01900000000002L114.0640625 65.01900000000002C120.73072916666668 65.01900000000002 124.0640625 68.35233333333335 124.0640625 75.01900000000002L124.0640625 75.01900000000002L124.0640625 120.03800000000001C124.0640625 126.70466666666667 120.73072916666668 130.038 114.0640625 130.038C114.0640625 130.038 120.7953125 130.038 120.7953125 130.038C114.12864583333334 130.038 110.7953125 126.70466666666667 110.7953125 120.03800000000001C110.7953125 120.03800000000001 110.7953125 120.03800000000001 110.7953125 120.03800000000001 "
                                                        fill="rgba(105,108,255,0.85)" fill-opacity="1"
                                                        stroke="#ffffff" stroke-opacity="1"
                                                        stroke-linecap="round" stroke-width="6"
                                                        stroke-dasharray="0"
                                                        class="apexcharts-bar-area" index="0"
                                                        clip-path="url(#gridRectMaskt035o2mbi)"
                                                        pathTo="M 110.7953125 120.03800000000001L 110.7953125 75.01900000000002Q 110.7953125 65.01900000000002 120.7953125 65.01900000000002L 114.0640625 65.01900000000002Q 124.0640625 65.01900000000002 124.0640625 75.01900000000002L 124.0640625 75.01900000000002L 124.0640625 120.03800000000001Q 124.0640625 130.038 114.0640625 130.038L 120.7953125 130.038Q 110.7953125 130.038 110.7953125 120.03800000000001z"
                                                        pathFrom="M 110.7953125 120.03800000000001L 110.7953125 120.03800000000001L 124.0640625 120.03800000000001L 124.0640625 120.03800000000001L 124.0640625 120.03800000000001L 124.0640625 120.03800000000001L 124.0640625 120.03800000000001L 110.7953125 120.03800000000001"
                                                        cy="65.01900000000002" cx="155.9671875"
                                                        j="2" val="15"
                                                        barHeight="65.01899999999999"
                                                        barWidth="19.26875">
                                                    </path>
                                                    <path id="SvgjsPath1020"
                                                        d="M158.9671875 120.03800000000001L158.9671875 14.334600000000023C158.9671875 7.667933333333352 162.30052083333334 4.334600000000023 168.9671875 4.334600000000023L162.2359375 4.334600000000023C168.90260416666666 4.334600000000023 172.2359375 7.667933333333352 172.2359375 14.334600000000023L172.2359375 14.334600000000023L172.2359375 120.03800000000001C172.2359375 126.70466666666667 168.90260416666666 130.038 162.2359375 130.038C162.2359375 130.038 168.9671875 130.038 168.9671875 130.038C162.30052083333334 130.038 158.9671875 126.70466666666667 158.9671875 120.03800000000001C158.9671875 120.03800000000001 158.9671875 120.03800000000001 158.9671875 120.03800000000001 "
                                                        fill="rgba(105,108,255,0.85)" fill-opacity="1"
                                                        stroke="#ffffff" stroke-opacity="1"
                                                        stroke-linecap="round" stroke-width="6"
                                                        stroke-dasharray="0"
                                                        class="apexcharts-bar-area" index="0"
                                                        clip-path="url(#gridRectMaskt035o2mbi)"
                                                        pathTo="M 158.9671875 120.03800000000001L 158.9671875 14.334600000000023Q 158.9671875 4.334600000000023 168.9671875 4.334600000000023L 162.2359375 4.334600000000023Q 172.2359375 4.334600000000023 172.2359375 14.334600000000023L 172.2359375 14.334600000000023L 172.2359375 120.03800000000001Q 172.2359375 130.038 162.2359375 130.038L 168.9671875 130.038Q 158.9671875 130.038 158.9671875 120.03800000000001z"
                                                        pathFrom="M 158.9671875 120.03800000000001L 158.9671875 120.03800000000001L 172.2359375 120.03800000000001L 172.2359375 120.03800000000001L 172.2359375 120.03800000000001L 172.2359375 120.03800000000001L 172.2359375 120.03800000000001L 158.9671875 120.03800000000001"
                                                        cy="4.334600000000023" cx="204.1390625"
                                                        j="3" val="29"
                                                        barHeight="125.70339999999999"
                                                        barWidth="19.26875"></path>
                                                    <path id="SvgjsPath1021"
                                                        d="M207.1390625 120.03800000000001L207.1390625 62.01520000000002C207.13906249999997 55.34853333333335 210.47239583333334 52.01520000000002 217.1390625 52.01520000000002L210.4078125 52.01520000000002C217.07447916666666 52.01520000000002 220.40781250000003 55.34853333333335 220.4078125 62.01520000000002L220.4078125 62.01520000000002L220.4078125 120.03800000000001C220.40781250000003 126.70466666666667 217.07447916666666 130.038 210.4078125 130.038C210.4078125 130.038 217.1390625 130.038 217.1390625 130.038C210.47239583333334 130.038 207.13906249999997 126.70466666666667 207.1390625 120.03800000000001C207.1390625 120.03800000000001 207.1390625 120.03800000000001 207.1390625 120.03800000000001 "
                                                        fill="rgba(105,108,255,0.85)" fill-opacity="1"
                                                        stroke="#ffffff" stroke-opacity="1"
                                                        stroke-linecap="round" stroke-width="6"
                                                        stroke-dasharray="0"
                                                        class="apexcharts-bar-area" index="0"
                                                        clip-path="url(#gridRectMaskt035o2mbi)"
                                                        pathTo="M 207.1390625 120.03800000000001L 207.1390625 62.01520000000002Q 207.1390625 52.01520000000002 217.1390625 52.01520000000002L 210.4078125 52.01520000000002Q 220.4078125 52.01520000000002 220.4078125 62.01520000000002L 220.4078125 62.01520000000002L 220.4078125 120.03800000000001Q 220.4078125 130.038 210.4078125 130.038L 217.1390625 130.038Q 207.1390625 130.038 207.1390625 120.03800000000001z"
                                                        pathFrom="M 207.1390625 120.03800000000001L 207.1390625 120.03800000000001L 220.4078125 120.03800000000001L 220.4078125 120.03800000000001L 220.4078125 120.03800000000001L 220.4078125 120.03800000000001L 220.4078125 120.03800000000001L 207.1390625 120.03800000000001"
                                                        cy="52.01520000000002" cx="252.3109375"
                                                        j="4" val="18"
                                                        barHeight="78.02279999999999"
                                                        barWidth="19.26875">
                                                    </path>
                                                    <path id="SvgjsPath1022"
                                                        d="M255.3109375 120.03800000000001L255.3109375 88.02280000000002C255.3109375 81.35613333333336 258.64427083333334 78.02280000000002 265.3109375 78.02280000000002L258.5796875 78.02280000000002C265.24635416666666 78.02280000000002 268.5796875 81.35613333333336 268.5796875 88.02280000000002L268.5796875 88.02280000000002L268.5796875 120.03800000000001C268.5796875 126.70466666666667 265.24635416666666 130.038 258.5796875 130.038C258.5796875 130.038 265.3109375 130.038 265.3109375 130.038C258.64427083333334 130.038 255.3109375 126.70466666666667 255.3109375 120.03800000000001C255.3109375 120.03800000000001 255.3109375 120.03800000000001 255.3109375 120.03800000000001 "
                                                        fill="rgba(105,108,255,0.85)" fill-opacity="1"
                                                        stroke="#ffffff" stroke-opacity="1"
                                                        stroke-linecap="round" stroke-width="6"
                                                        stroke-dasharray="0"
                                                        class="apexcharts-bar-area" index="0"
                                                        clip-path="url(#gridRectMaskt035o2mbi)"
                                                        pathTo="M 255.3109375 120.03800000000001L 255.3109375 88.02280000000002Q 255.3109375 78.02280000000002 265.3109375 78.02280000000002L 258.5796875 78.02280000000002Q 268.5796875 78.02280000000002 268.5796875 88.02280000000002L 268.5796875 88.02280000000002L 268.5796875 120.03800000000001Q 268.5796875 130.038 258.5796875 130.038L 265.3109375 130.038Q 255.3109375 130.038 255.3109375 120.03800000000001z"
                                                        pathFrom="M 255.3109375 120.03800000000001L 255.3109375 120.03800000000001L 268.5796875 120.03800000000001L 268.5796875 120.03800000000001L 268.5796875 120.03800000000001L 268.5796875 120.03800000000001L 268.5796875 120.03800000000001L 255.3109375 120.03800000000001"
                                                        cy="78.02280000000002" cx="300.4828125"
                                                        j="5" val="12"
                                                        barHeight="52.01519999999999"
                                                        barWidth="19.26875">
                                                    </path>
                                                    <path id="SvgjsPath1023"
                                                        d="M303.4828125 120.03800000000001L303.4828125 101.02660000000002C303.4828125 94.35993333333334 306.81614583333334 91.02660000000002 313.4828125 91.02660000000002L306.75156250000003 91.02660000000002C313.4182291666667 91.02660000000002 316.75156250000003 94.35993333333334 316.75156250000003 101.02660000000002L316.75156250000003 101.02660000000002L316.75156250000003 120.03800000000001C316.75156250000003 126.70466666666667 313.4182291666667 130.038 306.75156250000003 130.038C306.75156250000003 130.038 313.4828125 130.038 313.4828125 130.038C306.81614583333334 130.038 303.4828125 126.70466666666667 303.4828125 120.03800000000001C303.4828125 120.03800000000001 303.4828125 120.03800000000001 303.4828125 120.03800000000001 "
                                                        fill="rgba(105,108,255,0.85)" fill-opacity="1"
                                                        stroke="#ffffff" stroke-opacity="1"
                                                        stroke-linecap="round" stroke-width="6"
                                                        stroke-dasharray="0"
                                                        class="apexcharts-bar-area" index="0"
                                                        clip-path="url(#gridRectMaskt035o2mbi)"
                                                        pathTo="M 303.4828125 120.03800000000001L 303.4828125 101.02660000000002Q 303.4828125 91.02660000000002 313.4828125 91.02660000000002L 306.75156250000003 91.02660000000002Q 316.75156250000003 91.02660000000002 316.75156250000003 101.02660000000002L 316.75156250000003 101.02660000000002L 316.75156250000003 120.03800000000001Q 316.75156250000003 130.038 306.75156250000003 130.038L 313.4828125 130.038Q 303.4828125 130.038 303.4828125 120.03800000000001z"
                                                        pathFrom="M 303.4828125 120.03800000000001L 303.4828125 120.03800000000001L 316.75156250000003 120.03800000000001L 316.75156250000003 120.03800000000001L 316.75156250000003 120.03800000000001L 316.75156250000003 120.03800000000001L 316.75156250000003 120.03800000000001L 303.4828125 120.03800000000001"
                                                        cy="91.02660000000002" cx="348.6546875"
                                                        j="6" val="9"
                                                        barHeight="39.011399999999995"
                                                        barWidth="19.26875"></path>
                                                </g>
                                                <g id="SvgjsG1024" class="apexcharts-series"
                                                    seriesName="2020" rel="2"
                                                    data:realIndex="1">
                                                    <path id="SvgjsPath1026"
                                                        d="M14.4515625 150.038L14.4515625 186.3878C14.451562500000001 193.05446666666666 17.784895833333334 196.38779999999997 24.4515625 196.3878L17.7203125 196.3878C24.386979166666666 196.38779999999997 27.7203125 193.05446666666666 27.7203125 186.3878L27.7203125 186.3878L27.7203125 150.038C27.7203125 143.37133333333335 24.386979166666666 140.038 17.7203125 140.038C17.7203125 140.038 24.4515625 140.038 24.4515625 140.038C17.784895833333334 140.038 14.451562500000001 143.37133333333335 14.4515625 150.038C14.4515625 150.038 14.4515625 150.038 14.4515625 150.038 "
                                                        fill="rgba(3,195,236,0.85)" fill-opacity="1"
                                                        stroke="#ffffff" stroke-opacity="1"
                                                        stroke-linecap="round" stroke-width="6"
                                                        stroke-dasharray="0"
                                                        class="apexcharts-bar-area" index="1"
                                                        clip-path="url(#gridRectMaskt035o2mbi)"
                                                        pathTo="M 14.4515625 150.038L 14.4515625 186.3878Q 14.4515625 196.3878 24.4515625 196.3878L 17.7203125 196.3878Q 27.7203125 196.3878 27.7203125 186.3878L 27.7203125 186.3878L 27.7203125 150.038Q 27.7203125 140.038 17.7203125 140.038L 24.4515625 140.038Q 14.4515625 140.038 14.4515625 150.038z"
                                                        pathFrom="M 14.4515625 150.038L 14.4515625 150.038L 27.7203125 150.038L 27.7203125 150.038L 27.7203125 150.038L 27.7203125 150.038L 27.7203125 150.038L 14.4515625 150.038"
                                                        cy="176.3878" cx="59.6234375" j="0"
                                                        val="-13" barHeight="-56.349799999999995"
                                                        barWidth="19.26875"></path>
                                                    <path id="SvgjsPath1027"
                                                        d="M62.6234375 150.038L62.6234375 208.0608C62.6234375 214.72746666666666 65.95677083333334 218.06079999999997 72.6234375 218.0608L65.8921875 218.0608C72.55885416666668 218.06079999999997 75.8921875 214.72746666666666 75.8921875 208.0608L75.8921875 208.0608L75.8921875 150.038C75.8921875 143.37133333333335 72.55885416666668 140.038 65.8921875 140.038C65.8921875 140.038 72.6234375 140.038 72.6234375 140.038C65.95677083333334 140.038 62.6234375 143.37133333333335 62.6234375 150.038C62.6234375 150.038 62.6234375 150.038 62.6234375 150.038 "
                                                        fill="rgba(3,195,236,0.85)" fill-opacity="1"
                                                        stroke="#ffffff" stroke-opacity="1"
                                                        stroke-linecap="round" stroke-width="6"
                                                        stroke-dasharray="0"
                                                        class="apexcharts-bar-area" index="1"
                                                        clip-path="url(#gridRectMaskt035o2mbi)"
                                                        pathTo="M 62.6234375 150.038L 62.6234375 208.0608Q 62.6234375 218.0608 72.6234375 218.0608L 65.8921875 218.0608Q 75.8921875 218.0608 75.8921875 208.0608L 75.8921875 208.0608L 75.8921875 150.038Q 75.8921875 140.038 65.8921875 140.038L 72.6234375 140.038Q 62.6234375 140.038 62.6234375 150.038z"
                                                        pathFrom="M 62.6234375 150.038L 62.6234375 150.038L 75.8921875 150.038L 75.8921875 150.038L 75.8921875 150.038L 75.8921875 150.038L 75.8921875 150.038L 62.6234375 150.038"
                                                        cy="198.0608" cx="107.7953125"
                                                        j="1" val="-18"
                                                        barHeight="-78.02279999999999"
                                                        barWidth="19.26875"></path>
                                                    <path id="SvgjsPath1028"
                                                        d="M110.7953125 150.038L110.7953125 169.0494C110.7953125 175.71606666666665 114.12864583333334 179.0494 120.7953125 179.0494L114.0640625 179.0494C120.73072916666668 179.0494 124.0640625 175.71606666666665 124.0640625 169.0494L124.0640625 169.0494L124.0640625 150.038C124.0640625 143.37133333333335 120.73072916666668 140.038 114.0640625 140.038C114.0640625 140.038 120.7953125 140.038 120.7953125 140.038C114.12864583333334 140.038 110.7953125 143.37133333333335 110.7953125 150.038C110.7953125 150.038 110.7953125 150.038 110.7953125 150.038 "
                                                        fill="rgba(3,195,236,0.85)" fill-opacity="1"
                                                        stroke="#ffffff" stroke-opacity="1"
                                                        stroke-linecap="round" stroke-width="6"
                                                        stroke-dasharray="0"
                                                        class="apexcharts-bar-area" index="1"
                                                        clip-path="url(#gridRectMaskt035o2mbi)"
                                                        pathTo="M 110.7953125 150.038L 110.7953125 169.0494Q 110.7953125 179.0494 120.7953125 179.0494L 114.0640625 179.0494Q 124.0640625 179.0494 124.0640625 169.0494L 124.0640625 169.0494L 124.0640625 150.038Q 124.0640625 140.038 114.0640625 140.038L 120.7953125 140.038Q 110.7953125 140.038 110.7953125 150.038z"
                                                        pathFrom="M 110.7953125 150.038L 110.7953125 150.038L 124.0640625 150.038L 124.0640625 150.038L 124.0640625 150.038L 124.0640625 150.038L 124.0640625 150.038L 110.7953125 150.038"
                                                        cy="159.0494" cx="155.9671875"
                                                        j="2" val="-9"
                                                        barHeight="-39.011399999999995"
                                                        barWidth="19.26875"></path>
                                                    <path id="SvgjsPath1029"
                                                        d="M158.9671875 150.038L158.9671875 190.7224C158.9671875 197.38906666666665 162.30052083333334 200.7224 168.9671875 200.7224L162.2359375 200.7224C168.90260416666666 200.7224 172.2359375 197.38906666666665 172.2359375 190.7224L172.2359375 190.7224L172.2359375 150.038C172.2359375 143.37133333333335 168.90260416666666 140.038 162.2359375 140.038C162.2359375 140.038 168.9671875 140.038 168.9671875 140.038C162.30052083333334 140.038 158.9671875 143.37133333333335 158.9671875 150.038C158.9671875 150.038 158.9671875 150.038 158.9671875 150.038 "
                                                        fill="rgba(3,195,236,0.85)" fill-opacity="1"
                                                        stroke="#ffffff" stroke-opacity="1"
                                                        stroke-linecap="round" stroke-width="6"
                                                        stroke-dasharray="0"
                                                        class="apexcharts-bar-area" index="1"
                                                        clip-path="url(#gridRectMaskt035o2mbi)"
                                                        pathTo="M 158.9671875 150.038L 158.9671875 190.7224Q 158.9671875 200.7224 168.9671875 200.7224L 162.2359375 200.7224Q 172.2359375 200.7224 172.2359375 190.7224L 172.2359375 190.7224L 172.2359375 150.038Q 172.2359375 140.038 162.2359375 140.038L 168.9671875 140.038Q 158.9671875 140.038 158.9671875 150.038z"
                                                        pathFrom="M 158.9671875 150.038L 158.9671875 150.038L 172.2359375 150.038L 172.2359375 150.038L 172.2359375 150.038L 172.2359375 150.038L 172.2359375 150.038L 158.9671875 150.038"
                                                        cy="180.7224" cx="204.1390625"
                                                        j="3" val="-14"
                                                        barHeight="-60.6844" barWidth="19.26875">
                                                    </path>
                                                    <path id="SvgjsPath1030"
                                                        d="M207.1390625 150.038L207.1390625 151.711C207.13906249999997 158.3776666666667 210.47239583333334 161.711 217.1390625 161.711L210.4078125 161.711C217.07447916666666 161.711 220.40781250000003 158.3776666666667 220.4078125 151.711L220.4078125 151.711L220.4078125 150.038C220.40781250000003 143.37133333333335 217.07447916666666 140.038 210.4078125 140.038C210.4078125 140.038 217.1390625 140.038 217.1390625 140.038C210.47239583333334 140.038 207.13906249999997 143.37133333333335 207.1390625 150.038C207.1390625 150.038 207.1390625 150.038 207.1390625 150.038 "
                                                        fill="rgba(3,195,236,0.85)" fill-opacity="1"
                                                        stroke="#ffffff" stroke-opacity="1"
                                                        stroke-linecap="round" stroke-width="6"
                                                        stroke-dasharray="0"
                                                        class="apexcharts-bar-area" index="1"
                                                        clip-path="url(#gridRectMaskt035o2mbi)"
                                                        pathTo="M 207.1390625 150.038L 207.1390625 151.711Q 207.1390625 161.711 217.1390625 161.711L 210.4078125 161.711Q 220.4078125 161.711 220.4078125 151.711L 220.4078125 151.711L 220.4078125 150.038Q 220.4078125 140.038 210.4078125 140.038L 217.1390625 140.038Q 207.1390625 140.038 207.1390625 150.038z"
                                                        pathFrom="M 207.1390625 150.038L 207.1390625 150.038L 220.4078125 150.038L 220.4078125 150.038L 220.4078125 150.038L 220.4078125 150.038L 220.4078125 150.038L 207.1390625 150.038"
                                                        cy="141.711" cx="252.3109375"
                                                        j="4" val="-5"
                                                        barHeight="-21.673" barWidth="19.26875">
                                                    </path>
                                                    <path id="SvgjsPath1031"
                                                        d="M255.3109375 150.038L255.3109375 203.7262C255.3109375 210.3928666666667 258.64427083333334 213.7262 265.3109375 213.7262L258.5796875 213.7262C265.24635416666666 213.7262 268.5796875 210.3928666666667 268.5796875 203.7262L268.5796875 203.7262L268.5796875 150.038C268.5796875 143.37133333333335 265.24635416666666 140.038 258.5796875 140.038C258.5796875 140.038 265.3109375 140.038 265.3109375 140.038C258.64427083333334 140.038 255.3109375 143.37133333333335 255.3109375 150.038C255.3109375 150.038 255.3109375 150.038 255.3109375 150.038 "
                                                        fill="rgba(3,195,236,0.85)" fill-opacity="1"
                                                        stroke="#ffffff" stroke-opacity="1"
                                                        stroke-linecap="round" stroke-width="6"
                                                        stroke-dasharray="0"
                                                        class="apexcharts-bar-area" index="1"
                                                        clip-path="url(#gridRectMaskt035o2mbi)"
                                                        pathTo="M 255.3109375 150.038L 255.3109375 203.7262Q 255.3109375 213.7262 265.3109375 213.7262L 258.5796875 213.7262Q 268.5796875 213.7262 268.5796875 203.7262L 268.5796875 203.7262L 268.5796875 150.038Q 268.5796875 140.038 258.5796875 140.038L 265.3109375 140.038Q 255.3109375 140.038 255.3109375 150.038z"
                                                        pathFrom="M 255.3109375 150.038L 255.3109375 150.038L 268.5796875 150.038L 268.5796875 150.038L 268.5796875 150.038L 268.5796875 150.038L 268.5796875 150.038L 255.3109375 150.038"
                                                        cy="193.7262" cx="300.4828125"
                                                        j="5" val="-17"
                                                        barHeight="-73.6882" barWidth="19.26875">
                                                    </path>
                                                    <path id="SvgjsPath1032"
                                                        d="M303.4828125 150.038L303.4828125 195.05700000000002C303.4828125 201.72366666666667 306.81614583333334 205.05700000000002 313.4828125 205.05700000000002L306.75156250000003 205.05700000000002C313.4182291666667 205.05700000000002 316.75156250000003 201.72366666666667 316.75156250000003 195.05700000000002L316.75156250000003 195.05700000000002L316.75156250000003 150.038C316.75156250000003 143.37133333333335 313.4182291666667 140.038 306.75156250000003 140.038C306.75156250000003 140.038 313.4828125 140.038 313.4828125 140.038C306.81614583333334 140.038 303.4828125 143.37133333333335 303.4828125 150.038C303.4828125 150.038 303.4828125 150.038 303.4828125 150.038 "
                                                        fill="rgba(3,195,236,0.85)" fill-opacity="1"
                                                        stroke="#ffffff" stroke-opacity="1"
                                                        stroke-linecap="round" stroke-width="6"
                                                        stroke-dasharray="0"
                                                        class="apexcharts-bar-area" index="1"
                                                        clip-path="url(#gridRectMaskt035o2mbi)"
                                                        pathTo="M 303.4828125 150.038L 303.4828125 195.05700000000002Q 303.4828125 205.05700000000002 313.4828125 205.05700000000002L 306.75156250000003 205.05700000000002Q 316.75156250000003 205.05700000000002 316.75156250000003 195.05700000000002L 316.75156250000003 195.05700000000002L 316.75156250000003 150.038Q 316.75156250000003 140.038 306.75156250000003 140.038L 313.4828125 140.038Q 303.4828125 140.038 303.4828125 150.038z"
                                                        pathFrom="M 303.4828125 150.038L 303.4828125 150.038L 316.75156250000003 150.038L 316.75156250000003 150.038L 316.75156250000003 150.038L 316.75156250000003 150.038L 316.75156250000003 150.038L 303.4828125 150.038"
                                                        cy="185.05700000000002" cx="348.6546875"
                                                        j="6" val="-15"
                                                        barHeight="-65.01899999999999"
                                                        barWidth="19.26875"></path>
                                                </g>
                                                <g id="SvgjsG1016" class="apexcharts-datalabels"
                                                    data:realIndex="0"></g>
                                                <g id="SvgjsG1025" class="apexcharts-datalabels"
                                                    data:realIndex="1"></g>
                                            </g>
                                            <line id="SvgjsLine1081" x1="0" y1="0"
                                                x2="337.203125" y2="0" stroke="#b6b6b6"
                                                stroke-dasharray="0" stroke-width="1"
                                                stroke-linecap="butt" class="apexcharts-ycrosshairs">
                                            </line>
                                            <line id="SvgjsLine1082" x1="0" y1="0"
                                                x2="337.203125" y2="0" stroke-dasharray="0"
                                                stroke-width="0" stroke-linecap="butt"
                                                class="apexcharts-ycrosshairs-hidden"></line>
                                            <g id="SvgjsG1083" class="apexcharts-yaxis-annotations">
                                            </g>
                                            <g id="SvgjsG1084" class="apexcharts-xaxis-annotations">
                                            </g>
                                            <g id="SvgjsG1085" class="apexcharts-point-annotations">
                                            </g>
                                        </g>
                                        <g id="SvgjsG1056" class="apexcharts-yaxis" rel="0"
                                            transform="translate(15.796875, 0)">
                                            <g id="SvgjsG1057" class="apexcharts-yaxis-texts-g">
                                                <text id="SvgjsText1058"
                                                    font-family="Helvetica, Arial, sans-serif"
                                                    x="20" y="53.5" text-anchor="end"
                                                    dominant-baseline="auto" font-size="13px"
                                                    font-weight="400" fill="#373d3f"
                                                    class="apexcharts-text apexcharts-yaxis-label "
                                                    style="font-family: Helvetica, Arial, sans-serif;">
                                                    <tspan id="SvgjsTspan1059">30</tspan>
                                                    <title>30</title>
                                                </text><text id="SvgjsText1060"
                                                    font-family="Helvetica, Arial, sans-serif"
                                                    x="20" y="96.846" text-anchor="end"
                                                    dominant-baseline="auto" font-size="13px"
                                                    font-weight="400" fill="#373d3f"
                                                    class="apexcharts-text apexcharts-yaxis-label "
                                                    style="font-family: Helvetica, Arial, sans-serif;">
                                                    <tspan id="SvgjsTspan1061">20</tspan>
                                                    <title>20</title>
                                                </text><text id="SvgjsText1062"
                                                    font-family="Helvetica, Arial, sans-serif"
                                                    x="20" y="140.192" text-anchor="end"
                                                    dominant-baseline="auto" font-size="13px"
                                                    font-weight="400" fill="#373d3f"
                                                    class="apexcharts-text apexcharts-yaxis-label "
                                                    style="font-family: Helvetica, Arial, sans-serif;">
                                                    <tspan id="SvgjsTspan1063">10</tspan>
                                                    <title>10</title>
                                                </text><text id="SvgjsText1064"
                                                    font-family="Helvetica, Arial, sans-serif"
                                                    x="20" y="183.538" text-anchor="end"
                                                    dominant-baseline="auto" font-size="13px"
                                                    font-weight="400" fill="#373d3f"
                                                    class="apexcharts-text apexcharts-yaxis-label "
                                                    style="font-family: Helvetica, Arial, sans-serif;">
                                                    <tspan id="SvgjsTspan1065">0</tspan>
                                                    <title>0</title>
                                                </text><text id="SvgjsText1066"
                                                    font-family="Helvetica, Arial, sans-serif"
                                                    x="20" y="226.88400000000001"
                                                    text-anchor="end" dominant-baseline="auto"
                                                    font-size="13px" font-weight="400" fill="#373d3f"
                                                    class="apexcharts-text apexcharts-yaxis-label "
                                                    style="font-family: Helvetica, Arial, sans-serif;">
                                                    <tspan id="SvgjsTspan1067">-10</tspan>
                                                    <title>-10</title>
                                                </text><text id="SvgjsText1068"
                                                    font-family="Helvetica, Arial, sans-serif"
                                                    x="20" y="270.23" text-anchor="end"
                                                    dominant-baseline="auto" font-size="13px"
                                                    font-weight="400" fill="#373d3f"
                                                    class="apexcharts-text apexcharts-yaxis-label "
                                                    style="font-family: Helvetica, Arial, sans-serif;">
                                                    <tspan id="SvgjsTspan1069">-20</tspan>
                                                    <title>-20</title>
                                                </text>
                                            </g>
                                        </g>
                                        <g id="SvgjsG1004" class="apexcharts-annotations"></g>
                                    </svg>
                                    <div class="apexcharts-tooltip apexcharts-theme-light">
                                        <div class="apexcharts-tooltip-title"
                                            style="font-family: Helvetica, Arial, sans-serif; font-size: 12px;">
                                        </div>
                                        <div class="apexcharts-tooltip-series-group"
                                            style="order: 1;">
                                            <span class="apexcharts-tooltip-marker"
                                                style="background-color: rgb(105, 108, 255);"></span>
                                            <div class="apexcharts-tooltip-text"
                                                style="font-family: Helvetica, Arial, sans-serif; font-size: 12px;">
                                                <div class="apexcharts-tooltip-y-group"><span
                                                        class="apexcharts-tooltip-text-y-label"></span><span
                                                        class="apexcharts-tooltip-text-y-value"></span>
                                                </div>
                                                <div class="apexcharts-tooltip-goals-group"><span
                                                        class="apexcharts-tooltip-text-goals-label"></span><span
                                                        class="apexcharts-tooltip-text-goals-value"></span>
                                                </div>
                                                <div class="apexcharts-tooltip-z-group"><span
                                                        class="apexcharts-tooltip-text-z-label"></span><span
                                                        class="apexcharts-tooltip-text-z-value"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="apexcharts-tooltip-series-group"
                                            style="order: 2;">
                                            <span class="apexcharts-tooltip-marker"
                                                style="background-color: rgb(3, 195, 236);"></span>
                                            <div class="apexcharts-tooltip-text"
                                                style="font-family: Helvetica, Arial, sans-serif; font-size: 12px;">
                                                <div class="apexcharts-tooltip-y-group"><span
                                                        class="apexcharts-tooltip-text-y-label"></span><span
                                                        class="apexcharts-tooltip-text-y-value"></span>
                                                </div>
                                                <div class="apexcharts-tooltip-goals-group"><span
                                                        class="apexcharts-tooltip-text-goals-label"></span><span
                                                        class="apexcharts-tooltip-text-goals-value"></span>
                                                </div>
                                                <div class="apexcharts-tooltip-z-group"><span
                                                        class="apexcharts-tooltip-text-z-label"></span><span
                                                        class="apexcharts-tooltip-text-z-value"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        class="apexcharts-yaxistooltip apexcharts-yaxistooltip-0 apexcharts-yaxistooltip-left apexcharts-theme-light">
                                        <div class="apexcharts-yaxistooltip-text"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="resize-triggers">
                                <div class="expand-trigger">
                                    <div style="width: 428px; height: 377px;"></div>
                                </div>
                                <div class="contract-trigger"></div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card-body">
                                <div class="text-center">
                                    <div class="dropdown">
                                        <button class="btn btn-sm btn-outline-primary dropdown-toggle"
                                            type="button" id="growthReportId"
                                            data-bs-toggle="dropdown" aria-haspopup="true"
                                            aria-expanded="false">
                                            2022
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-end"
                                            aria-labelledby="growthReportId">
                                            <a class="dropdown-item"
                                                href="javascript:void(0);">2021</a>
                                            <a class="dropdown-item"
                                                href="javascript:void(0);">2020</a>
                                            <a class="dropdown-item"
                                                href="javascript:void(0);">2019</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="growthChart" style="min-height: 154.875px;">
                                <div id="apexchartsdmyzuqbof"
                                    class="apexcharts-canvas apexchartsdmyzuqbof apexcharts-theme-light"
                                    style="width: 214px; height: 154.875px;"><svg id="SvgjsSvg1086"
                                        width="214" height="154.875"
                                        xmlns="http://www.w3.org/2000/svg" version="1.1"
                                        xmlns:xlink="http://www.w3.org/1999/xlink"
                                        xmlns:svgjs="http://svgjs.dev" class="apexcharts-svg"
                                        xmlns:data="ApexChartsNS" transform="translate(0, 0)"
                                        style="background: transparent;">
                                        <g id="SvgjsG1088"
                                            class="apexcharts-inner apexcharts-graphical"
                                            transform="translate(0, -25)">
                                            <defs id="SvgjsDefs1087">
                                                <clipPath id="gridRectMaskdmyzuqbof">
                                                    <rect id="SvgjsRect1090" width="222"
                                                        height="285" x="-3" y="-1"
                                                        rx="0" ry="0" opacity="1"
                                                        stroke-width="0" stroke="none"
                                                        stroke-dasharray="0" fill="#fff"></rect>
                                                </clipPath>
                                                <clipPath id="forecastMaskdmyzuqbof"></clipPath>
                                                <clipPath id="nonForecastMaskdmyzuqbof"></clipPath>
                                                <clipPath id="gridRectMarkerMaskdmyzuqbof">
                                                    <rect id="SvgjsRect1091" width="220"
                                                        height="287" x="-2" y="-2"
                                                        rx="0" ry="0" opacity="1"
                                                        stroke-width="0" stroke="none"
                                                        stroke-dasharray="0" fill="#fff"></rect>
                                                </clipPath>
                                                <linearGradient id="SvgjsLinearGradient1096"
                                                    x1="1" y1="0" x2="0"
                                                    y2="1">
                                                    <stop id="SvgjsStop1097" stop-opacity="1"
                                                        stop-color="rgba(105,108,255,1)"
                                                        offset="0.3">
                                                    </stop>
                                                    <stop id="SvgjsStop1098" stop-opacity="0.6"
                                                        stop-color="rgba(255,255,255,0.6)"
                                                        offset="0.7"></stop>
                                                    <stop id="SvgjsStop1099" stop-opacity="0.6"
                                                        stop-color="rgba(255,255,255,0.6)"
                                                        offset="1"></stop>
                                                </linearGradient>
                                                <linearGradient id="SvgjsLinearGradient1107"
                                                    x1="1" y1="0" x2="0"
                                                    y2="1">
                                                    <stop id="SvgjsStop1108" stop-opacity="1"
                                                        stop-color="rgba(105,108,255,1)"
                                                        offset="0.3">
                                                    </stop>
                                                    <stop id="SvgjsStop1109" stop-opacity="0.6"
                                                        stop-color="rgba(105,108,255,0.6)"
                                                        offset="0.7"></stop>
                                                    <stop id="SvgjsStop1110" stop-opacity="0.6"
                                                        stop-color="rgba(105,108,255,0.6)"
                                                        offset="1"></stop>
                                                </linearGradient>
                                            </defs>
                                            <g id="SvgjsG1092" class="apexcharts-radialbar">
                                                <g id="SvgjsG1093">
                                                    <g id="SvgjsG1094" class="apexcharts-tracks">
                                                        <g id="SvgjsG1095"
                                                            class="apexcharts-radialbar-track apexcharts-track"
                                                            rel="1">
                                                            <path id="apexcharts-radialbarTrack-0"
                                                                d="M 73.83506097560974 167.17541022773656 A 68.32987804878049 68.32987804878049 0 1 1 142.16493902439026 167.17541022773656"
                                                                fill="none" fill-opacity="1"
                                                                stroke="rgba(255,255,255,0.85)"
                                                                stroke-opacity="1"
                                                                stroke-linecap="butt"
                                                                stroke-width="17.357317073170734"
                                                                stroke-dasharray="0"
                                                                class="apexcharts-radialbar-area"
                                                                data:pathOrig="M 73.83506097560974 167.17541022773656 A 68.32987804878049 68.32987804878049 0 1 1 142.16493902439026 167.17541022773656">
                                                            </path>
                                                        </g>
                                                    </g>
                                                    <g id="SvgjsG1101">
                                                        <g id="SvgjsG1106"
                                                            class="apexcharts-series apexcharts-radial-series"
                                                            seriesName="Growth" rel="1"
                                                            data:realIndex="0">
                                                            <path id="SvgjsPath1111"
                                                                d="M 73.83506097560974 167.17541022773656 A 68.32987804878049 68.32987804878049 0 1 1 175.95555982735613 100.85758285229481"
                                                                fill="none" fill-opacity="0.85"
                                                                stroke="url(#SvgjsLinearGradient1107)"
                                                                stroke-opacity="1"
                                                                stroke-linecap="butt"
                                                                stroke-width="17.357317073170734"
                                                                stroke-dasharray="5"
                                                                class="apexcharts-radialbar-area apexcharts-radialbar-slice-0"
                                                                data:angle="234" data:value="78"
                                                                index="0" j="0"
                                                                data:pathOrig="M 73.83506097560974 167.17541022773656 A 68.32987804878049 68.32987804878049 0 1 1 175.95555982735613 100.85758285229481">
                                                            </path>
                                                        </g>
                                                        <circle id="SvgjsCircle1102"
                                                            r="54.65121951219512" cx="108"
                                                            cy="108"
                                                            class="apexcharts-radialbar-hollow"
                                                            fill="transparent"></circle>
                                                        <g id="SvgjsG1103"
                                                            class="apexcharts-datalabels-group"
                                                            transform="translate(0, 0) scale(1)"
                                                            style="opacity: 1;"><text
                                                                id="SvgjsText1104"
                                                                font-family="Public Sans"
                                                                x="108" y="123"
                                                                text-anchor="middle"
                                                                dominant-baseline="auto"
                                                                font-size="15px" font-weight="500"
                                                                fill="#566a7f"
                                                                class="apexcharts-text apexcharts-datalabel-label"
                                                                style="font-family: &quot;Public Sans&quot;;">Growth</text><text
                                                                id="SvgjsText1105"
                                                                font-family="Public Sans"
                                                                x="108" y="99"
                                                                text-anchor="middle"
                                                                dominant-baseline="auto"
                                                                font-size="22px" font-weight="500"
                                                                fill="#566a7f"
                                                                class="apexcharts-text apexcharts-datalabel-value"
                                                                style="font-family: &quot;Public Sans&quot;;">78%</text>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                            <line id="SvgjsLine1112" x1="0" y1="0"
                                                x2="216" y2="0" stroke="#b6b6b6"
                                                stroke-dasharray="0" stroke-width="1"
                                                stroke-linecap="butt"
                                                class="apexcharts-ycrosshairs">
                                            </line>
                                            <line id="SvgjsLine1113" x1="0" y1="0"
                                                x2="216" y2="0" stroke-dasharray="0"
                                                stroke-width="0" stroke-linecap="butt"
                                                class="apexcharts-ycrosshairs-hidden"></line>
                                        </g>
                                        <g id="SvgjsG1089" class="apexcharts-annotations"></g>
                                    </svg>
                                    <div class="apexcharts-legend"></div>
                                </div>
                            </div>
                            <div class="text-center fw-medium pt-3 mb-2">62% Company Growth</div>

                            <div
                                class="d-flex px-xxl-4 px-lg-2 p-4 gap-xxl-3 gap-lg-1 gap-3 justify-content-between">
                                <div class="d-flex">
                                    <div class="me-2">
                                        <span class="badge bg-label-primary p-2"><i
                                                class="bx bx-dollar text-primary"></i></span>
                                    </div>
                                    <div class="d-flex flex-column">
                                        <small>2022</small>
                                        <h6 class="mb-0">$32.5k</h6>
                                    </div>
                                </div>
                                <div class="d-flex">
                                    <div class="me-2">
                                        <span class="badge bg-label-info p-2"><i
                                                class="bx bx-wallet text-info"></i></span>
                                    </div>
                                    <div class="d-flex flex-column">
                                        <small>2021</small>
                                        <h6 class="mb-0">$41.2k</h6>
                                    </div>
                                </div>
                            </div>
                            <div class="resize-triggers">
                                <div class="expand-trigger">
                                    <div style="width: 215px; height: 377px;"></div>
                                </div>
                                <div class="contract-trigger"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ Total Revenue -->
            <div class="col-12 col-md-8 col-lg-4 order-3 order-md-2">
                <div class="row">
                    <div class="col-6 mb-4">
                        <div class="card">
                            <div class="card-body">
                                <div
                                    class="card-title d-flex align-items-start justify-content-between">
                                    <div class="avatar flex-shrink-0">
                                        <img src="../assets/img/icons/unicons/paypal.png"
                                            alt="Credit Card" class="rounded">
                                    </div>
                                    <div class="dropdown">
                                        <button class="btn p-0" type="button" id="cardOpt4"
                                            data-bs-toggle="dropdown" aria-haspopup="true"
                                            aria-expanded="false">
                                            <i class="bx bx-dots-vertical-rounded"></i>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-end"
                                            aria-labelledby="cardOpt4">
                                            <a class="dropdown-item" href="javascript:void(0);">View
                                                More</a>
                                            <a class="dropdown-item"
                                                href="javascript:void(0);">Delete</a>
                                        </div>
                                    </div>
                                </div>
                                <span class="d-block mb-1">Payments</span>
                                <h3 class="card-title text-nowrap mb-2">$2,456</h3>
                                <small class="text-danger fw-medium"><i
                                        class="bx bx-down-arrow-alt"></i> -14.82%</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 mb-4">
                        <div class="card">
                            <div class="card-body">
                                <div
                                    class="card-title d-flex align-items-start justify-content-between">
                                    <div class="avatar flex-shrink-0">
                                        <img src="../assets/img/icons/unicons/cc-primary.png"
                                            alt="Credit Card" class="rounded">
                                    </div>
                                    <div class="dropdown">
                                        <button class="btn p-0" type="button" id="cardOpt1"
                                            data-bs-toggle="dropdown" aria-haspopup="true"
                                            aria-expanded="false">
                                            <i class="bx bx-dots-vertical-rounded"></i>
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="cardOpt1">
                                            <a class="dropdown-item" href="javascript:void(0);">View
                                                More</a>
                                            <a class="dropdown-item"
                                                href="javascript:void(0);">Delete</a>
                                        </div>
                                    </div>
                                </div>
                                <span class="fw-medium d-block mb-1">Transactions</span>
                                <h3 class="card-title mb-2">$14,857</h3>
                                <small class="text-success fw-medium"><i
                                        class="bx bx-up-arrow-alt"></i>
                                    +28.14%</small>
                            </div>
                        </div>
                    </div>
                    <!-- </div>
<div class="row"> -->
                    <div class="col-12 mb-4">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex justify-content-between flex-sm-row flex-column gap-3"
                                    style="position: relative;">
                                    <div
                                        class="d-flex flex-sm-column flex-row align-items-start justify-content-between">
                                        <div class="card-title">
                                            <h5 class="text-nowrap mb-2">Profile Report</h5>
                                            <span class="badge bg-label-warning rounded-pill">Year
                                                2021</span>
                                        </div>
                                        <div class="mt-sm-auto">
                                            <small class="text-success text-nowrap fw-medium"><i
                                                    class="bx bx-chevron-up"></i> 68.2%</small>
                                            <h3 class="mb-0">$84,686k</h3>
                                        </div>
                                    </div>
                                    <div id="profileReportChart" style="min-height: 80px;">
                                        <div id="apexchartslwe6kd3r"
                                            class="apexcharts-canvas apexchartslwe6kd3r apexcharts-theme-light"
                                            style="width: 128px; height: 80px;"><svg
                                                id="SvgjsSvg1114" width="128" height="80"
                                                xmlns="http://www.w3.org/2000/svg" version="1.1"
                                                xmlns:xlink="http://www.w3.org/1999/xlink"
                                                xmlns:svgjs="http://svgjs.dev"
                                                class="apexcharts-svg" xmlns:data="ApexChartsNS"
                                                transform="translate(0, 0)"
                                                style="background: transparent;">
                                                <g id="SvgjsG1116"
                                                    class="apexcharts-inner apexcharts-graphical"
                                                    transform="translate(0, 0)">
                                                    <defs id="SvgjsDefs1115">
                                                        <clipPath id="gridRectMasklwe6kd3r">
                                                            <rect id="SvgjsRect1121" width="129"
                                                                height="85" x="-4.5"
                                                                y="-2.5" rx="0"
                                                                ry="0" opacity="1"
                                                                stroke-width="0" stroke="none"
                                                                stroke-dasharray="0" fill="#fff">
                                                            </rect>
                                                        </clipPath>
                                                        <clipPath id="forecastMasklwe6kd3r">
                                                        </clipPath>
                                                        <clipPath id="nonForecastMasklwe6kd3r">
                                                        </clipPath>
                                                        <clipPath id="gridRectMarkerMasklwe6kd3r">
                                                            <rect id="SvgjsRect1122" width="124"
                                                                height="84" x="-2"
                                                                y="-2" rx="0"
                                                                ry="0" opacity="1"
                                                                stroke-width="0" stroke="none"
                                                                stroke-dasharray="0" fill="#fff">
                                                            </rect>
                                                        </clipPath>
                                                        <filter id="SvgjsFilter1128"
                                                            filterUnits="userSpaceOnUse"
                                                            width="200%" height="200%"
                                                            x="-50%" y="-50%">
                                                            <feFlood id="SvgjsFeFlood1129"
                                                                flood-color="#ffab00"
                                                                flood-opacity="0.15"
                                                                result="SvgjsFeFlood1129Out"
                                                                in="SourceGraphic"></feFlood>
                                                            <feComposite id="SvgjsFeComposite1130"
                                                                in="SvgjsFeFlood1129Out"
                                                                in2="SourceAlpha" operator="in"
                                                                result="SvgjsFeComposite1130Out">
                                                            </feComposite>
                                                            <feOffset id="SvgjsFeOffset1131"
                                                                dx="5" dy="10"
                                                                result="SvgjsFeOffset1131Out"
                                                                in="SvgjsFeComposite1130Out">
                                                            </feOffset>
                                                            <feGaussianBlur
                                                                id="SvgjsFeGaussianBlur1132"
                                                                stdDeviation="3 "
                                                                result="SvgjsFeGaussianBlur1132Out"
                                                                in="SvgjsFeOffset1131Out">
                                                            </feGaussianBlur>
                                                            <feMerge id="SvgjsFeMerge1133"
                                                                result="SvgjsFeMerge1133Out"
                                                                in="SourceGraphic">
                                                                <feMergeNode id="SvgjsFeMergeNode1134"
                                                                    in="SvgjsFeGaussianBlur1132Out">
                                                                </feMergeNode>
                                                                <feMergeNode id="SvgjsFeMergeNode1135"
                                                                    in="[object Arguments]">
                                                                </feMergeNode>
                                                            </feMerge>
                                                            <feBlend id="SvgjsFeBlend1136"
                                                                in="SourceGraphic"
                                                                in2="SvgjsFeMerge1133Out"
                                                                mode="normal"
                                                                result="SvgjsFeBlend1136Out">
                                                            </feBlend>
                                                        </filter>
                                                    </defs>
                                                    <line id="SvgjsLine1120" x1="0"
                                                        y1="0" x2="0"
                                                        y2="80" stroke="#b6b6b6"
                                                        stroke-dasharray="3" stroke-linecap="butt"
                                                        class="apexcharts-xcrosshairs"
                                                        x="0" y="0"
                                                        width="1" height="80"
                                                        fill="#b1b9c4" filter="none"
                                                        fill-opacity="0.9" stroke-width="1"></line>
                                                    <g id="SvgjsG1137" class="apexcharts-xaxis"
                                                        transform="translate(0, 0)">
                                                        <g id="SvgjsG1138"
                                                            class="apexcharts-xaxis-texts-g"
                                                            transform="translate(0, -4)"></g>
                                                    </g>
                                                    <g id="SvgjsG1146" class="apexcharts-grid">
                                                        <g id="SvgjsG1147"
                                                            class="apexcharts-gridlines-horizontal"
                                                            style="display: none;">
                                                            <line id="SvgjsLine1149" x1="0"
                                                                y1="0" x2="120"
                                                                y2="0" stroke="#e0e0e0"
                                                                stroke-dasharray="0"
                                                                stroke-linecap="butt"
                                                                class="apexcharts-gridline"></line>
                                                            <line id="SvgjsLine1150" x1="0"
                                                                y1="20" x2="120"
                                                                y2="20" stroke="#e0e0e0"
                                                                stroke-dasharray="0"
                                                                stroke-linecap="butt"
                                                                class="apexcharts-gridline"></line>
                                                            <line id="SvgjsLine1151" x1="0"
                                                                y1="40" x2="120"
                                                                y2="40" stroke="#e0e0e0"
                                                                stroke-dasharray="0"
                                                                stroke-linecap="butt"
                                                                class="apexcharts-gridline"></line>
                                                            <line id="SvgjsLine1152" x1="0"
                                                                y1="60" x2="120"
                                                                y2="60" stroke="#e0e0e0"
                                                                stroke-dasharray="0"
                                                                stroke-linecap="butt"
                                                                class="apexcharts-gridline"></line>
                                                            <line id="SvgjsLine1153" x1="0"
                                                                y1="80" x2="120"
                                                                y2="80" stroke="#e0e0e0"
                                                                stroke-dasharray="0"
                                                                stroke-linecap="butt"
                                                                class="apexcharts-gridline"></line>
                                                        </g>
                                                        <g id="SvgjsG1148"
                                                            class="apexcharts-gridlines-vertical"
                                                            style="display: none;"></g>
                                                        <line id="SvgjsLine1155" x1="0"
                                                            y1="80" x2="120"
                                                            y2="80" stroke="transparent"
                                                            stroke-dasharray="0"
                                                            stroke-linecap="butt">
                                                        </line>
                                                        <line id="SvgjsLine1154" x1="0"
                                                            y1="1" x2="0"
                                                            y2="80" stroke="transparent"
                                                            stroke-dasharray="0"
                                                            stroke-linecap="butt">
                                                        </line>
                                                    </g>
                                                    <g id="SvgjsG1123"
                                                        class="apexcharts-line-series apexcharts-plot-series">
                                                        <g id="SvgjsG1124" class="apexcharts-series"
                                                            seriesName="seriesx1"
                                                            data:longestSeries="true" rel="1"
                                                            data:realIndex="0">
                                                            <path id="SvgjsPath1127"
                                                                d="M0 76C8.399999999999999 76 15.600000000000001 12 24 12C32.4 12 39.6 62 48 62C56.4 62 63.6 22 72 22C80.4 22 87.6 38 96 38C104.4 38 111.6 6 120 6C120 6 120 6 120 6 "
                                                                fill="none" fill-opacity="1"
                                                                stroke="rgba(255,171,0,0.85)"
                                                                stroke-opacity="1"
                                                                stroke-linecap="butt"
                                                                stroke-width="5"
                                                                stroke-dasharray="0"
                                                                class="apexcharts-line"
                                                                index="0"
                                                                clip-path="url(#gridRectMasklwe6kd3r)"
                                                                filter="url(#SvgjsFilter1128)"
                                                                pathTo="M 0 76C 8.399999999999999 76 15.600000000000001 12 24 12C 32.4 12 39.6 62 48 62C 56.4 62 63.6 22 72 22C 80.4 22 87.6 38 96 38C 104.4 38 111.6 6 120 6"
                                                                pathFrom="M -1 120L -1 120L 24 120L 48 120L 72 120L 96 120L 120 120">
                                                            </path>
                                                            <g id="SvgjsG1125"
                                                                class="apexcharts-series-markers-wrap"
                                                                data:realIndex="0">
                                                                <g class="apexcharts-series-markers">
                                                                    <circle id="SvgjsCircle1161"
                                                                        r="0"
                                                                        cx="0"
                                                                        cy="0"
                                                                        class="apexcharts-marker w3odeoopf no-pointer-events"
                                                                        stroke="#ffffff"
                                                                        fill="#ffab00"
                                                                        fill-opacity="1"
                                                                        stroke-width="2"
                                                                        stroke-opacity="0.9"
                                                                        default-marker-size="0">
                                                                    </circle>
                                                                </g>
                                                            </g>
                                                        </g>
                                                        <g id="SvgjsG1126"
                                                            class="apexcharts-datalabels"
                                                            data:realIndex="0"></g>
                                                    </g>
                                                    <line id="SvgjsLine1156" x1="0"
                                                        y1="0" x2="120"
                                                        y2="0" stroke="#b6b6b6"
                                                        stroke-dasharray="0" stroke-width="1"
                                                        stroke-linecap="butt"
                                                        class="apexcharts-ycrosshairs"></line>
                                                    <line id="SvgjsLine1157" x1="0"
                                                        y1="0" x2="120"
                                                        y2="0" stroke-dasharray="0"
                                                        stroke-width="0" stroke-linecap="butt"
                                                        class="apexcharts-ycrosshairs-hidden"></line>
                                                    <g id="SvgjsG1158"
                                                        class="apexcharts-yaxis-annotations"></g>
                                                    <g id="SvgjsG1159"
                                                        class="apexcharts-xaxis-annotations"></g>
                                                    <g id="SvgjsG1160"
                                                        class="apexcharts-point-annotations"></g>
                                                </g>
                                                <rect id="SvgjsRect1119" width="0"
                                                    height="0" x="0" y="0"
                                                    rx="0" ry="0" opacity="1"
                                                    stroke-width="0" stroke="none"
                                                    stroke-dasharray="0" fill="#fefefe"></rect>
                                                <g id="SvgjsG1145" class="apexcharts-yaxis"
                                                    rel="0" transform="translate(-18, 0)">
                                                </g>
                                                <g id="SvgjsG1117" class="apexcharts-annotations">
                                                </g>
                                            </svg>
                                            <div class="apexcharts-legend"
                                                style="max-height: 40px;">
                                            </div>
                                            <div class="apexcharts-tooltip apexcharts-theme-light">
                                                <div class="apexcharts-tooltip-title"
                                                    style="font-family: Helvetica, Arial, sans-serif; font-size: 12px;">
                                                </div>
                                                <div class="apexcharts-tooltip-series-group"
                                                    style="order: 1;"><span
                                                        class="apexcharts-tooltip-marker"
                                                        style="background-color: rgb(255, 171, 0);"></span>
                                                    <div class="apexcharts-tooltip-text"
                                                        style="font-family: Helvetica, Arial, sans-serif; font-size: 12px;">
                                                        <div class="apexcharts-tooltip-y-group"><span
                                                                class="apexcharts-tooltip-text-y-label"></span><span
                                                                class="apexcharts-tooltip-text-y-value"></span>
                                                        </div>
                                                        <div class="apexcharts-tooltip-goals-group">
                                                            <span
                                                                class="apexcharts-tooltip-text-goals-label"></span><span
                                                                class="apexcharts-tooltip-text-goals-value"></span>
                                                        </div>
                                                        <div class="apexcharts-tooltip-z-group"><span
                                                                class="apexcharts-tooltip-text-z-label"></span><span
                                                                class="apexcharts-tooltip-text-z-value"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div
                                                class="apexcharts-yaxistooltip apexcharts-yaxistooltip-0 apexcharts-yaxistooltip-left apexcharts-theme-light">
                                                <div class="apexcharts-yaxistooltip-text"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="resize-triggers">
                                        <div class="expand-trigger">
                                            <div style="width: 260px; height: 116px;"></div>
                                        </div>
                                        <div class="contract-trigger"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <!-- Order Statistics -->
            <div class="col-md-6 col-lg-4 col-xl-4 order-0 mb-4">
                <div class="card h-100">
                    <div class="card-header d-flex align-items-center justify-content-between pb-0">
                        <div class="card-title mb-0">
                            <h5 class="m-0 me-2">Order Statistics</h5>
                            <small class="text-muted">42.82k Total Sales</small>
                        </div>
                        <div class="dropdown">
                            <button class="btn p-0" type="button" id="orederStatistics"
                                data-bs-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                                <i class="bx bx-dots-vertical-rounded"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-end"
                                aria-labelledby="orederStatistics">
                                <a class="dropdown-item" href="javascript:void(0);">Select All</a>
                                <a class="dropdown-item" href="javascript:void(0);">Refresh</a>
                                <a class="dropdown-item" href="javascript:void(0);">Share</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="d-flex justify-content-between align-items-center mb-3"
                            style="position: relative;">
                            <div class="d-flex flex-column align-items-center gap-1">
                                <h2 class="mb-2">8,258</h2>
                                <span>Total Orders</span>
                            </div>
                            <div id="orderStatisticsChart" style="min-height: 137.55px;">
                                <div id="apexchartsbet9sifm"
                                    class="apexcharts-canvas apexchartsbet9sifm apexcharts-theme-light"
                                    style="width: 130px; height: 137.55px;"><svg id="SvgjsSvg1162"
                                        width="130" height="137.55"
                                        xmlns="http://www.w3.org/2000/svg" version="1.1"
                                        xmlns:xlink="http://www.w3.org/1999/xlink"
                                        xmlns:svgjs="http://svgjs.dev" class="apexcharts-svg"
                                        xmlns:data="ApexChartsNS" transform="translate(0, 0)"
                                        style="background: transparent;">
                                        <g id="SvgjsG1164"
                                            class="apexcharts-inner apexcharts-graphical"
                                            transform="translate(-7, 0)">
                                            <defs id="SvgjsDefs1163">
                                                <clipPath id="gridRectMaskbet9sifm">
                                                    <rect id="SvgjsRect1166" width="150"
                                                        height="173" x="-4.5"
                                                        y="-2.5" rx="0"
                                                        ry="0" opacity="1"
                                                        stroke-width="0" stroke="none"
                                                        stroke-dasharray="0" fill="#fff"></rect>
                                                </clipPath>
                                                <clipPath id="forecastMaskbet9sifm"></clipPath>
                                                <clipPath id="nonForecastMaskbet9sifm"></clipPath>
                                                <clipPath id="gridRectMarkerMaskbet9sifm">
                                                    <rect id="SvgjsRect1167" width="145"
                                                        height="172" x="-2"
                                                        y="-2" rx="0"
                                                        ry="0" opacity="1"
                                                        stroke-width="0" stroke="none"
                                                        stroke-dasharray="0" fill="#fff"></rect>
                                                </clipPath>
                                            </defs>
                                            <g id="SvgjsG1168" class="apexcharts-pie">
                                                <g id="SvgjsG1169"
                                                    transform="translate(0, 0) scale(1)">
                                                    <circle id="SvgjsCircle1170"
                                                        r="44.835365853658544" cx="70.5"
                                                        cy="70.5" fill="transparent"></circle>
                                                    <g id="SvgjsG1171" class="apexcharts-slices">
                                                        <g id="SvgjsG1172"
                                                            class="apexcharts-series apexcharts-pie-series"
                                                            seriesName="Electronic" rel="1"
                                                            data:realIndex="0">
                                                            <path id="SvgjsPath1173"
                                                                d="M 70.5 10.71951219512195 A 59.78048780487805 59.78048780487805 0 0 1 97.63977353321047 123.7648046533095 L 90.85483014990785 110.44860348998213 A 44.835365853658544 44.835365853658544 0 0 0 70.5 25.664634146341456 L 70.5 10.71951219512195 z"
                                                                fill="rgba(105,108,255,1)"
                                                                fill-opacity="1" stroke-opacity="1"
                                                                stroke-linecap="butt"
                                                                stroke-width="5"
                                                                stroke-dasharray="0"
                                                                class="apexcharts-pie-area apexcharts-donut-slice-0"
                                                                index="0" j="0"
                                                                data:angle="153" data:startAngle="0"
                                                                data:strokeWidth="5" data:value="85"
                                                                data:pathOrig="M 70.5 10.71951219512195 A 59.78048780487805 59.78048780487805 0 0 1 97.63977353321047 123.7648046533095 L 90.85483014990785 110.44860348998213 A 44.835365853658544 44.835365853658544 0 0 0 70.5 25.664634146341456 L 70.5 10.71951219512195 z"
                                                                stroke="#ffffff"></path>
                                                        </g>
                                                        <g id="SvgjsG1174"
                                                            class="apexcharts-series apexcharts-pie-series"
                                                            seriesName="Sports" rel="2"
                                                            data:realIndex="1">
                                                            <path id="SvgjsPath1175"
                                                                d="M 97.63977353321047 123.7648046533095 A 59.78048780487805 59.78048780487805 0 0 1 70.5 130.28048780487805 L 70.5 115.33536585365854 A 44.835365853658544 44.835365853658544 0 0 0 90.85483014990785 110.44860348998213 L 97.63977353321047 123.7648046533095 z"
                                                                fill="rgba(133,146,163,1)"
                                                                fill-opacity="1" stroke-opacity="1"
                                                                stroke-linecap="butt"
                                                                stroke-width="5"
                                                                stroke-dasharray="0"
                                                                class="apexcharts-pie-area apexcharts-donut-slice-1"
                                                                index="0" j="1"
                                                                data:angle="27"
                                                                data:startAngle="153"
                                                                data:strokeWidth="5" data:value="15"
                                                                data:pathOrig="M 97.63977353321047 123.7648046533095 A 59.78048780487805 59.78048780487805 0 0 1 70.5 130.28048780487805 L 70.5 115.33536585365854 A 44.835365853658544 44.835365853658544 0 0 0 90.85483014990785 110.44860348998213 L 97.63977353321047 123.7648046533095 z"
                                                                stroke="#ffffff"></path>
                                                        </g>
                                                        <g id="SvgjsG1176"
                                                            class="apexcharts-series apexcharts-pie-series"
                                                            seriesName="Decor" rel="3"
                                                            data:realIndex="2">
                                                            <path id="SvgjsPath1177"
                                                                d="M 70.5 130.28048780487805 A 59.78048780487805 59.78048780487805 0 0 1 10.71951219512195 70.50000000000001 L 25.664634146341456 70.5 A 44.835365853658544 44.835365853658544 0 0 0 70.5 115.33536585365854 L 70.5 130.28048780487805 z"
                                                                fill="rgba(3,195,236,1)"
                                                                fill-opacity="1" stroke-opacity="1"
                                                                stroke-linecap="butt"
                                                                stroke-width="5"
                                                                stroke-dasharray="0"
                                                                class="apexcharts-pie-area apexcharts-donut-slice-2"
                                                                index="0" j="2"
                                                                data:angle="90"
                                                                data:startAngle="180"
                                                                data:strokeWidth="5" data:value="50"
                                                                data:pathOrig="M 70.5 130.28048780487805 A 59.78048780487805 59.78048780487805 0 0 1 10.71951219512195 70.50000000000001 L 25.664634146341456 70.5 A 44.835365853658544 44.835365853658544 0 0 0 70.5 115.33536585365854 L 70.5 130.28048780487805 z"
                                                                stroke="#ffffff"></path>
                                                        </g>
                                                        <g id="SvgjsG1178"
                                                            class="apexcharts-series apexcharts-pie-series"
                                                            seriesName="Fashion" rel="4"
                                                            data:realIndex="3">
                                                            <path id="SvgjsPath1179"
                                                                d="M 10.71951219512195 70.50000000000001 A 59.78048780487805 59.78048780487805 0 0 1 70.48956633664653 10.719513105630845 L 70.4921747524849 25.664634829223125 A 44.835365853658544 44.835365853658544 0 0 0 25.664634146341456 70.5 L 10.71951219512195 70.50000000000001 z"
                                                                fill="rgba(113,221,55,1)"
                                                                fill-opacity="1" stroke-opacity="1"
                                                                stroke-linecap="butt"
                                                                stroke-width="5"
                                                                stroke-dasharray="0"
                                                                class="apexcharts-pie-area apexcharts-donut-slice-3"
                                                                index="0" j="3"
                                                                data:angle="90"
                                                                data:startAngle="270"
                                                                data:strokeWidth="5" data:value="50"
                                                                data:pathOrig="M 10.71951219512195 70.50000000000001 A 59.78048780487805 59.78048780487805 0 0 1 70.48956633664653 10.719513105630845 L 70.4921747524849 25.664634829223125 A 44.835365853658544 44.835365853658544 0 0 0 25.664634146341456 70.5 L 10.71951219512195 70.50000000000001 z"
                                                                stroke="#ffffff"></path>
                                                        </g>
                                                    </g>
                                                </g>
                                                <g id="SvgjsG1180"
                                                    class="apexcharts-datalabels-group"
                                                    transform="translate(0, 0) scale(1)"><text
                                                        id="SvgjsText1181"
                                                        font-family="Helvetica, Arial, sans-serif"
                                                        x="70.5" y="90.5"
                                                        text-anchor="middle"
                                                        dominant-baseline="auto"
                                                        font-size="0.8125rem" font-weight="400"
                                                        fill="#373d3f"
                                                        class="apexcharts-text apexcharts-datalabel-label"
                                                        style="font-family: Helvetica, Arial, sans-serif;">Weekly</text><text
                                                        id="SvgjsText1182" font-family="Public Sans"
                                                        x="70.5" y="71.5"
                                                        text-anchor="middle"
                                                        dominant-baseline="auto" font-size="1.5rem"
                                                        font-weight="400" fill="#566a7f"
                                                        class="apexcharts-text apexcharts-datalabel-value"
                                                        style="font-family: &quot;Public Sans&quot;;">38%</text>
                                                </g>
                                            </g>
                                            <line id="SvgjsLine1183" x1="0" y1="0"
                                                x2="141" y2="0" stroke="#b6b6b6"
                                                stroke-dasharray="0" stroke-width="1"
                                                stroke-linecap="butt"
                                                class="apexcharts-ycrosshairs">
                                            </line>
                                            <line id="SvgjsLine1184" x1="0" y1="0"
                                                x2="141" y2="0" stroke-dasharray="0"
                                                stroke-width="0" stroke-linecap="butt"
                                                class="apexcharts-ycrosshairs-hidden"></line>
                                        </g>
                                        <g id="SvgjsG1165" class="apexcharts-annotations"></g>
                                    </svg>
                                    <div class="apexcharts-legend"></div>
                                    <div class="apexcharts-tooltip apexcharts-theme-dark">
                                        <div class="apexcharts-tooltip-series-group"
                                            style="order: 1;">
                                            <span class="apexcharts-tooltip-marker"
                                                style="background-color: rgb(105, 108, 255);"></span>
                                            <div class="apexcharts-tooltip-text"
                                                style="font-family: Helvetica, Arial, sans-serif; font-size: 12px;">
                                                <div class="apexcharts-tooltip-y-group"><span
                                                        class="apexcharts-tooltip-text-y-label"></span><span
                                                        class="apexcharts-tooltip-text-y-value"></span>
                                                </div>
                                                <div class="apexcharts-tooltip-goals-group"><span
                                                        class="apexcharts-tooltip-text-goals-label"></span><span
                                                        class="apexcharts-tooltip-text-goals-value"></span>
                                                </div>
                                                <div class="apexcharts-tooltip-z-group"><span
                                                        class="apexcharts-tooltip-text-z-label"></span><span
                                                        class="apexcharts-tooltip-text-z-value"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="apexcharts-tooltip-series-group"
                                            style="order: 2;">
                                            <span class="apexcharts-tooltip-marker"
                                                style="background-color: rgb(133, 146, 163);"></span>
                                            <div class="apexcharts-tooltip-text"
                                                style="font-family: Helvetica, Arial, sans-serif; font-size: 12px;">
                                                <div class="apexcharts-tooltip-y-group"><span
                                                        class="apexcharts-tooltip-text-y-label"></span><span
                                                        class="apexcharts-tooltip-text-y-value"></span>
                                                </div>
                                                <div class="apexcharts-tooltip-goals-group"><span
                                                        class="apexcharts-tooltip-text-goals-label"></span><span
                                                        class="apexcharts-tooltip-text-goals-value"></span>
                                                </div>
                                                <div class="apexcharts-tooltip-z-group"><span
                                                        class="apexcharts-tooltip-text-z-label"></span><span
                                                        class="apexcharts-tooltip-text-z-value"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="apexcharts-tooltip-series-group"
                                            style="order: 3;">
                                            <span class="apexcharts-tooltip-marker"
                                                style="background-color: rgb(3, 195, 236);"></span>
                                            <div class="apexcharts-tooltip-text"
                                                style="font-family: Helvetica, Arial, sans-serif; font-size: 12px;">
                                                <div class="apexcharts-tooltip-y-group"><span
                                                        class="apexcharts-tooltip-text-y-label"></span><span
                                                        class="apexcharts-tooltip-text-y-value"></span>
                                                </div>
                                                <div class="apexcharts-tooltip-goals-group"><span
                                                        class="apexcharts-tooltip-text-goals-label"></span><span
                                                        class="apexcharts-tooltip-text-goals-value"></span>
                                                </div>
                                                <div class="apexcharts-tooltip-z-group"><span
                                                        class="apexcharts-tooltip-text-z-label"></span><span
                                                        class="apexcharts-tooltip-text-z-value"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="apexcharts-tooltip-series-group"
                                            style="order: 4;">
                                            <span class="apexcharts-tooltip-marker"
                                                style="background-color: rgb(113, 221, 55);"></span>
                                            <div class="apexcharts-tooltip-text"
                                                style="font-family: Helvetica, Arial, sans-serif; font-size: 12px;">
                                                <div class="apexcharts-tooltip-y-group"><span
                                                        class="apexcharts-tooltip-text-y-label"></span><span
                                                        class="apexcharts-tooltip-text-y-value"></span>
                                                </div>
                                                <div class="apexcharts-tooltip-goals-group"><span
                                                        class="apexcharts-tooltip-text-goals-label"></span><span
                                                        class="apexcharts-tooltip-text-goals-value"></span>
                                                </div>
                                                <div class="apexcharts-tooltip-z-group"><span
                                                        class="apexcharts-tooltip-text-z-label"></span><span
                                                        class="apexcharts-tooltip-text-z-value"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="resize-triggers">
                                <div class="expand-trigger">
                                    <div style="width: 260px; height: 139px;"></div>
                                </div>
                                <div class="contract-trigger"></div>
                            </div>
                        </div>
                        <ul class="p-0 m-0">
                            <li class="d-flex mb-4 pb-1">
                                <div class="avatar flex-shrink-0 me-3">
                                    <span class="avatar-initial rounded bg-label-primary"><i
                                            class="bx bx-mobile-alt"></i></span>
                                </div>
                                <div
                                    class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                                    <div class="me-2">
                                        <h6 class="mb-0">Electronic</h6>
                                        <small class="text-muted">Mobile, Earbuds, TV</small>
                                    </div>
                                    <div class="user-progress">
                                        <small class="fw-medium">82.5k</small>
                                    </div>
                                </div>
                            </li>
                            <li class="d-flex mb-4 pb-1">
                                <div class="avatar flex-shrink-0 me-3">
                                    <span class="avatar-initial rounded bg-label-success"><i
                                            class="bx bx-closet"></i></span>
                                </div>
                                <div
                                    class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                                    <div class="me-2">
                                        <h6 class="mb-0">Fashion</h6>
                                        <small class="text-muted">T-shirt, Jeans, Shoes</small>
                                    </div>
                                    <div class="user-progress">
                                        <small class="fw-medium">23.8k</small>
                                    </div>
                                </div>
                            </li>
                            <li class="d-flex mb-4 pb-1">
                                <div class="avatar flex-shrink-0 me-3">
                                    <span class="avatar-initial rounded bg-label-info"><i
                                            class="bx bx-home-alt"></i></span>
                                </div>
                                <div
                                    class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                                    <div class="me-2">
                                        <h6 class="mb-0">Decor</h6>
                                        <small class="text-muted">Fine Art, Dining</small>
                                    </div>
                                    <div class="user-progress">
                                        <small class="fw-medium">849k</small>
                                    </div>
                                </div>
                            </li>
                            <li class="d-flex">
                                <div class="avatar flex-shrink-0 me-3">
                                    <span class="avatar-initial rounded bg-label-secondary"><i
                                            class="bx bx-football"></i></span>
                                </div>
                                <div
                                    class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                                    <div class="me-2">
                                        <h6 class="mb-0">Sports</h6>
                                        <small class="text-muted">Football, Cricket Kit</small>
                                    </div>
                                    <div class="user-progress">
                                        <small class="fw-medium">99</small>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--/ Order Statistics -->

            <!-- Expense Overview -->
            <div class="col-md-6 col-lg-4 order-1 mb-4">
                <div class="card h-100">
                    <div class="card-header">
                        <ul class="nav nav-pills" role="tablist">
                            <li class="nav-item" role="presentation">
                                <button type="button" class="nav-link active" role="tab"
                                    data-bs-toggle="tab"
                                    data-bs-target="#navs-tabs-line-card-income"
                                    aria-controls="navs-tabs-line-card-income" aria-selected="true">
                                    Income
                                </button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button type="button" class="nav-link" role="tab"
                                    aria-selected="false" tabindex="-1">Expenses</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button type="button" class="nav-link" role="tab"
                                    aria-selected="false" tabindex="-1">Profit</button>
                            </li>
                        </ul>
                    </div>
                    <div class="card-body px-0">
                        <div class="tab-content p-0">
                            <div class="tab-pane fade show active" id="navs-tabs-line-card-income"
                                role="tabpanel" style="position: relative;">
                                <div class="d-flex p-4 pt-3">
                                    <div class="avatar flex-shrink-0 me-3">
                                        <img src="../assets/img/icons/unicons/wallet.png"
                                            alt="User">
                                    </div>
                                    <div>
                                        <small class="text-muted d-block">Total Balance</small>
                                        <div class="d-flex align-items-center">
                                            <h6 class="mb-0 me-1">$459.10</h6>
                                            <small class="text-success fw-medium">
                                                <i class="bx bx-chevron-up"></i>
                                                42.9%
                                            </small>
                                        </div>
                                    </div>
                                </div>
                                <div id="incomeChart" style="min-height: 215px;">
                                    <div id="apexchartsoaqqldxp"
                                        class="apexcharts-canvas apexchartsoaqqldxp apexcharts-theme-light"
                                        style="width: 307px; height: 215px;"><svg id="SvgjsSvg1185"
                                            width="307" height="215"
                                            xmlns="http://www.w3.org/2000/svg" version="1.1"
                                            xmlns:xlink="http://www.w3.org/1999/xlink"
                                            xmlns:svgjs="http://svgjs.dev"
                                            class="apexcharts-svg apexcharts-zoomable"
                                            xmlns:data="ApexChartsNS" transform="translate(0, 0)"
                                            style="background: transparent;">
                                            <g id="SvgjsG1187"
                                                class="apexcharts-inner apexcharts-graphical"
                                                transform="translate(0, 10)">
                                                <defs id="SvgjsDefs1186">
                                                    <clipPath id="gridRectMaskoaqqldxp">
                                                        <rect id="SvgjsRect1192" width="295.6875"
                                                            height="175.73" x="-3"
                                                            y="-1" rx="0"
                                                            ry="0" opacity="1"
                                                            stroke-width="0" stroke="none"
                                                            stroke-dasharray="0" fill="#fff">
                                                        </rect>
                                                    </clipPath>
                                                    <clipPath id="forecastMaskoaqqldxp"></clipPath>
                                                    <clipPath id="nonForecastMaskoaqqldxp"></clipPath>
                                                    <clipPath id="gridRectMarkerMaskoaqqldxp">
                                                        <rect id="SvgjsRect1193" width="317.6875"
                                                            height="201.73" x="-14"
                                                            y="-14" rx="0"
                                                            ry="0" opacity="1"
                                                            stroke-width="0" stroke="none"
                                                            stroke-dasharray="0" fill="#fff">
                                                        </rect>
                                                    </clipPath>
                                                    <linearGradient id="SvgjsLinearGradient1213"
                                                        x1="0" y1="0"
                                                        x2="0" y2="1">
                                                        <stop id="SvgjsStop1214" stop-opacity="0.5"
                                                            stop-color="rgba(105,108,255,0.5)"
                                                            offset="0"></stop>
                                                        <stop id="SvgjsStop1215" stop-opacity="0.25"
                                                            stop-color="rgba(195,196,255,0.25)"
                                                            offset="0.95"></stop>
                                                        <stop id="SvgjsStop1216" stop-opacity="0.25"
                                                            stop-color="rgba(195,196,255,0.25)"
                                                            offset="1"></stop>
                                                    </linearGradient>
                                                </defs>
                                                <line id="SvgjsLine1191" x1="0"
                                                    y1="0" x2="0" y2="173.73"
                                                    stroke="#b6b6b6" stroke-dasharray="3"
                                                    stroke-linecap="butt"
                                                    class="apexcharts-xcrosshairs" x="0"
                                                    y="0" width="1" height="173.73"
                                                    fill="#b1b9c4" filter="none"
                                                    fill-opacity="0.9" stroke-width="1"></line>
                                                <g id="SvgjsG1219" class="apexcharts-xaxis"
                                                    transform="translate(0, 0)">
                                                    <g id="SvgjsG1220"
                                                        class="apexcharts-xaxis-texts-g"
                                                        transform="translate(0, -4)"><text
                                                            id="SvgjsText1222"
                                                            font-family="Helvetica, Arial, sans-serif"
                                                            x="0" y="202.73"
                                                            text-anchor="middle"
                                                            dominant-baseline="auto"
                                                            font-size="13px" font-weight="400"
                                                            fill="#373d3f"
                                                            class="apexcharts-text apexcharts-xaxis-label "
                                                            style="font-family: Helvetica, Arial, sans-serif;">
                                                            <tspan id="SvgjsTspan1223"></tspan>
                                                            <title></title>
                                                        </text><text id="SvgjsText1225"
                                                            font-family="Helvetica, Arial, sans-serif"
                                                            x="41.38392857142857" y="202.73"
                                                            text-anchor="middle"
                                                            dominant-baseline="auto"
                                                            font-size="13px" font-weight="400"
                                                            fill="#373d3f"
                                                            class="apexcharts-text apexcharts-xaxis-label "
                                                            style="font-family: Helvetica, Arial, sans-serif;">
                                                            <tspan id="SvgjsTspan1226">Jan</tspan>
                                                            <title>Jan</title>
                                                        </text><text id="SvgjsText1228"
                                                            font-family="Helvetica, Arial, sans-serif"
                                                            x="82.76785714285714" y="202.73"
                                                            text-anchor="middle"
                                                            dominant-baseline="auto"
                                                            font-size="13px" font-weight="400"
                                                            fill="#373d3f"
                                                            class="apexcharts-text apexcharts-xaxis-label "
                                                            style="font-family: Helvetica, Arial, sans-serif;">
                                                            <tspan id="SvgjsTspan1229">Feb</tspan>
                                                            <title>Feb</title>
                                                        </text><text id="SvgjsText1231"
                                                            font-family="Helvetica, Arial, sans-serif"
                                                            x="124.15178571428572" y="202.73"
                                                            text-anchor="middle"
                                                            dominant-baseline="auto"
                                                            font-size="13px" font-weight="400"
                                                            fill="#373d3f"
                                                            class="apexcharts-text apexcharts-xaxis-label "
                                                            style="font-family: Helvetica, Arial, sans-serif;">
                                                            <tspan id="SvgjsTspan1232">Mar</tspan>
                                                            <title>Mar</title>
                                                        </text><text id="SvgjsText1234"
                                                            font-family="Helvetica, Arial, sans-serif"
                                                            x="165.53571428571428" y="202.73"
                                                            text-anchor="middle"
                                                            dominant-baseline="auto"
                                                            font-size="13px" font-weight="400"
                                                            fill="#373d3f"
                                                            class="apexcharts-text apexcharts-xaxis-label "
                                                            style="font-family: Helvetica, Arial, sans-serif;">
                                                            <tspan id="SvgjsTspan1235">Apr</tspan>
                                                            <title>Apr</title>
                                                        </text><text id="SvgjsText1237"
                                                            font-family="Helvetica, Arial, sans-serif"
                                                            x="206.91964285714283" y="202.73"
                                                            text-anchor="middle"
                                                            dominant-baseline="auto"
                                                            font-size="13px" font-weight="400"
                                                            fill="#373d3f"
                                                            class="apexcharts-text apexcharts-xaxis-label "
                                                            style="font-family: Helvetica, Arial, sans-serif;">
                                                            <tspan id="SvgjsTspan1238">May</tspan>
                                                            <title>May</title>
                                                        </text><text id="SvgjsText1240"
                                                            font-family="Helvetica, Arial, sans-serif"
                                                            x="248.3035714285714" y="202.73"
                                                            text-anchor="middle"
                                                            dominant-baseline="auto"
                                                            font-size="13px" font-weight="400"
                                                            fill="#373d3f"
                                                            class="apexcharts-text apexcharts-xaxis-label "
                                                            style="font-family: Helvetica, Arial, sans-serif;">
                                                            <tspan id="SvgjsTspan1241">Jun</tspan>
                                                            <title>Jun</title>
                                                        </text><text id="SvgjsText1243"
                                                            font-family="Helvetica, Arial, sans-serif"
                                                            x="289.68749999999994" y="202.73"
                                                            text-anchor="middle"
                                                            dominant-baseline="auto"
                                                            font-size="13px" font-weight="400"
                                                            fill="#373d3f"
                                                            class="apexcharts-text apexcharts-xaxis-label "
                                                            style="font-family: Helvetica, Arial, sans-serif;">
                                                            <tspan id="SvgjsTspan1244">Jul</tspan>
                                                            <title>Jul</title>
                                                        </text></g>
                                                </g>
                                                <g id="SvgjsG1247" class="apexcharts-grid">
                                                    <g id="SvgjsG1248"
                                                        class="apexcharts-gridlines-horizontal">
                                                        <line id="SvgjsLine1250" x1="0"
                                                            y1="0" x2="289.6875"
                                                            y2="0" stroke="#eceef1"
                                                            stroke-dasharray="3"
                                                            stroke-linecap="butt"
                                                            class="apexcharts-gridline"></line>
                                                        <line id="SvgjsLine1251" x1="0"
                                                            y1="43.4325" x2="289.6875"
                                                            y2="43.4325" stroke="#eceef1"
                                                            stroke-dasharray="3"
                                                            stroke-linecap="butt"
                                                            class="apexcharts-gridline"></line>
                                                        <line id="SvgjsLine1252" x1="0"
                                                            y1="86.865" x2="289.6875"
                                                            y2="86.865" stroke="#eceef1"
                                                            stroke-dasharray="3"
                                                            stroke-linecap="butt"
                                                            class="apexcharts-gridline"></line>
                                                        <line id="SvgjsLine1253" x1="0"
                                                            y1="130.29749999999999" x2="289.6875"
                                                            y2="130.29749999999999" stroke="#eceef1"
                                                            stroke-dasharray="3"
                                                            stroke-linecap="butt"
                                                            class="apexcharts-gridline"></line>
                                                        <line id="SvgjsLine1254" x1="0"
                                                            y1="173.73" x2="289.6875"
                                                            y2="173.73" stroke="#eceef1"
                                                            stroke-dasharray="3"
                                                            stroke-linecap="butt"
                                                            class="apexcharts-gridline"></line>
                                                    </g>
                                                    <g id="SvgjsG1249"
                                                        class="apexcharts-gridlines-vertical"></g>
                                                    <line id="SvgjsLine1256" x1="0"
                                                        y1="173.73" x2="289.6875"
                                                        y2="173.73" stroke="transparent"
                                                        stroke-dasharray="0" stroke-linecap="butt">
                                                    </line>
                                                    <line id="SvgjsLine1255" x1="0"
                                                        y1="1" x2="0"
                                                        y2="173.73" stroke="transparent"
                                                        stroke-dasharray="0" stroke-linecap="butt">
                                                    </line>
                                                </g>
                                                <g id="SvgjsG1194"
                                                    class="apexcharts-area-series apexcharts-plot-series">
                                                    <g id="SvgjsG1195" class="apexcharts-series"
                                                        seriesName="seriesx1"
                                                        data:longestSeries="true" rel="1"
                                                        data:realIndex="0">
                                                        <path id="SvgjsPath1217"
                                                            d="M0 173.73L0 112.92450000000001C14.484375 112.92450000000001 26.899553571428577 125.95425 41.38392857142858 125.95425C55.86830357142858 125.95425 68.28348214285715 86.86500000000001 82.76785714285715 86.86500000000001C97.25223214285715 86.86500000000001 109.66741071428572 121.611 124.15178571428572 121.611C138.63616071428572 121.611 151.0513392857143 34.74600000000001 165.5357142857143 34.74600000000001C180.0200892857143 34.74600000000001 192.43526785714286 104.238 206.91964285714286 104.238C221.40401785714286 104.238 233.81919642857144 65.14875 248.30357142857144 65.14875C262.78794642857144 65.14875 275.203125 91.20825 289.6875 91.20825C289.6875 91.20825 289.6875 91.20825 289.6875 173.73M289.6875 91.20825C289.6875 91.20825 289.6875 91.20825 289.6875 91.20825 "
                                                            fill="url(#SvgjsLinearGradient1213)"
                                                            fill-opacity="1" stroke-opacity="1"
                                                            stroke-linecap="butt" stroke-width="0"
                                                            stroke-dasharray="0"
                                                            class="apexcharts-area" index="0"
                                                            clip-path="url(#gridRectMaskoaqqldxp)"
                                                            pathTo="M 0 173.73L 0 112.92450000000001C 14.484375 112.92450000000001 26.899553571428577 125.95425 41.38392857142858 125.95425C 55.86830357142858 125.95425 68.28348214285715 86.86500000000001 82.76785714285715 86.86500000000001C 97.25223214285715 86.86500000000001 109.66741071428572 121.611 124.15178571428572 121.611C 138.63616071428572 121.611 151.0513392857143 34.74600000000001 165.5357142857143 34.74600000000001C 180.0200892857143 34.74600000000001 192.43526785714286 104.238 206.91964285714286 104.238C 221.40401785714286 104.238 233.81919642857144 65.14875 248.30357142857144 65.14875C 262.78794642857144 65.14875 275.203125 91.20825 289.6875 91.20825C 289.6875 91.20825 289.6875 91.20825 289.6875 173.73M 289.6875 91.20825z"
                                                            pathFrom="M -1 217.1625L -1 217.1625L 41.38392857142858 217.1625L 82.76785714285715 217.1625L 124.15178571428572 217.1625L 165.5357142857143 217.1625L 206.91964285714286 217.1625L 248.30357142857144 217.1625L 289.6875 217.1625">
                                                        </path>
                                                        <path id="SvgjsPath1218"
                                                            d="M0 112.92450000000001C14.484375 112.92450000000001 26.899553571428577 125.95425 41.38392857142858 125.95425C55.86830357142858 125.95425 68.28348214285715 86.86500000000001 82.76785714285715 86.86500000000001C97.25223214285715 86.86500000000001 109.66741071428572 121.611 124.15178571428572 121.611C138.63616071428572 121.611 151.0513392857143 34.74600000000001 165.5357142857143 34.74600000000001C180.0200892857143 34.74600000000001 192.43526785714286 104.238 206.91964285714286 104.238C221.40401785714286 104.238 233.81919642857144 65.14875 248.30357142857144 65.14875C262.78794642857144 65.14875 275.203125 91.20825 289.6875 91.20825C289.6875 91.20825 289.6875 91.20825 289.6875 91.20825 "
                                                            fill="none" fill-opacity="1"
                                                            stroke="#696cff" stroke-opacity="1"
                                                            stroke-linecap="butt" stroke-width="2"
                                                            stroke-dasharray="0"
                                                            class="apexcharts-area" index="0"
                                                            clip-path="url(#gridRectMaskoaqqldxp)"
                                                            pathTo="M 0 112.92450000000001C 14.484375 112.92450000000001 26.899553571428577 125.95425 41.38392857142858 125.95425C 55.86830357142858 125.95425 68.28348214285715 86.86500000000001 82.76785714285715 86.86500000000001C 97.25223214285715 86.86500000000001 109.66741071428572 121.611 124.15178571428572 121.611C 138.63616071428572 121.611 151.0513392857143 34.74600000000001 165.5357142857143 34.74600000000001C 180.0200892857143 34.74600000000001 192.43526785714286 104.238 206.91964285714286 104.238C 221.40401785714286 104.238 233.81919642857144 65.14875 248.30357142857144 65.14875C 262.78794642857144 65.14875 275.203125 91.20825 289.6875 91.20825"
                                                            pathFrom="M -1 217.1625L -1 217.1625L 41.38392857142858 217.1625L 82.76785714285715 217.1625L 124.15178571428572 217.1625L 165.5357142857143 217.1625L 206.91964285714286 217.1625L 248.30357142857144 217.1625L 289.6875 217.1625">
                                                        </path>
                                                        <g id="SvgjsG1196"
                                                            class="apexcharts-series-markers-wrap"
                                                            data:realIndex="0">
                                                            <g id="SvgjsG1198"
                                                                class="apexcharts-series-markers"
                                                                clip-path="url(#gridRectMarkerMaskoaqqldxp)">
                                                                <circle id="SvgjsCircle1199"
                                                                    r="6" cx="0"
                                                                    cy="112.92450000000001"
                                                                    class="apexcharts-marker no-pointer-events w5rvwynp8"
                                                                    stroke="transparent"
                                                                    fill="transparent"
                                                                    fill-opacity="1"
                                                                    stroke-width="4"
                                                                    stroke-opacity="0.9"
                                                                    rel="0" j="0"
                                                                    index="0"
                                                                    default-marker-size="6"></circle>
                                                                <circle id="SvgjsCircle1200"
                                                                    r="6"
                                                                    cx="41.38392857142858"
                                                                    cy="125.95425"
                                                                    class="apexcharts-marker no-pointer-events woywk262i"
                                                                    stroke="transparent"
                                                                    fill="transparent"
                                                                    fill-opacity="1"
                                                                    stroke-width="4"
                                                                    stroke-opacity="0.9"
                                                                    rel="1" j="1"
                                                                    index="0"
                                                                    default-marker-size="6"></circle>
                                                            </g>
                                                            <g id="SvgjsG1201"
                                                                class="apexcharts-series-markers"
                                                                clip-path="url(#gridRectMarkerMaskoaqqldxp)">
                                                                <circle id="SvgjsCircle1202"
                                                                    r="6"
                                                                    cx="82.76785714285715"
                                                                    cy="86.86500000000001"
                                                                    class="apexcharts-marker no-pointer-events w11u0z5g1"
                                                                    stroke="transparent"
                                                                    fill="transparent"
                                                                    fill-opacity="1"
                                                                    stroke-width="4"
                                                                    stroke-opacity="0.9"
                                                                    rel="2" j="2"
                                                                    index="0"
                                                                    default-marker-size="6"></circle>
                                                            </g>
                                                            <g id="SvgjsG1203"
                                                                class="apexcharts-series-markers"
                                                                clip-path="url(#gridRectMarkerMaskoaqqldxp)">
                                                                <circle id="SvgjsCircle1204"
                                                                    r="6"
                                                                    cx="124.15178571428572"
                                                                    cy="121.611"
                                                                    class="apexcharts-marker no-pointer-events wtgpaubyn"
                                                                    stroke="transparent"
                                                                    fill="transparent"
                                                                    fill-opacity="1"
                                                                    stroke-width="4"
                                                                    stroke-opacity="0.9"
                                                                    rel="3" j="3"
                                                                    index="0"
                                                                    default-marker-size="6"></circle>
                                                            </g>
                                                            <g id="SvgjsG1205"
                                                                class="apexcharts-series-markers"
                                                                clip-path="url(#gridRectMarkerMaskoaqqldxp)">
                                                                <circle id="SvgjsCircle1206"
                                                                    r="6"
                                                                    cx="165.5357142857143"
                                                                    cy="34.74600000000001"
                                                                    class="apexcharts-marker no-pointer-events wos71q45f"
                                                                    stroke="transparent"
                                                                    fill="transparent"
                                                                    fill-opacity="1"
                                                                    stroke-width="4"
                                                                    stroke-opacity="0.9"
                                                                    rel="4" j="4"
                                                                    index="0"
                                                                    default-marker-size="6"></circle>
                                                            </g>
                                                            <g id="SvgjsG1207"
                                                                class="apexcharts-series-markers"
                                                                clip-path="url(#gridRectMarkerMaskoaqqldxp)">
                                                                <circle id="SvgjsCircle1208"
                                                                    r="6"
                                                                    cx="206.91964285714286"
                                                                    cy="104.238"
                                                                    class="apexcharts-marker no-pointer-events wsn29mknjl"
                                                                    stroke="transparent"
                                                                    fill="transparent"
                                                                    fill-opacity="1"
                                                                    stroke-width="4"
                                                                    stroke-opacity="0.9"
                                                                    rel="5" j="5"
                                                                    index="0"
                                                                    default-marker-size="6"></circle>
                                                            </g>
                                                            <g id="SvgjsG1209"
                                                                class="apexcharts-series-markers"
                                                                clip-path="url(#gridRectMarkerMaskoaqqldxp)">
                                                                <circle id="SvgjsCircle1210"
                                                                    r="6"
                                                                    cx="248.30357142857144"
                                                                    cy="65.14875"
                                                                    class="apexcharts-marker no-pointer-events wvz7mhh4v"
                                                                    stroke="transparent"
                                                                    fill="transparent"
                                                                    fill-opacity="1"
                                                                    stroke-width="4"
                                                                    stroke-opacity="0.9"
                                                                    rel="6" j="6"
                                                                    index="0"
                                                                    default-marker-size="6"></circle>
                                                            </g>
                                                            <g id="SvgjsG1211"
                                                                class="apexcharts-series-markers"
                                                                clip-path="url(#gridRectMarkerMaskoaqqldxp)">
                                                                <circle id="SvgjsCircle1212"
                                                                    r="6" cx="289.6875"
                                                                    cy="91.20825"
                                                                    class="apexcharts-marker no-pointer-events w4z4lmb3w"
                                                                    stroke="#696cff" fill="#ffffff"
                                                                    fill-opacity="1"
                                                                    stroke-width="4"
                                                                    stroke-opacity="0.9"
                                                                    rel="7" j="7"
                                                                    index="0"
                                                                    default-marker-size="6"></circle>
                                                            </g>
                                                        </g>
                                                    </g>
                                                    <g id="SvgjsG1197" class="apexcharts-datalabels"
                                                        data:realIndex="0"></g>
                                                </g>
                                                <line id="SvgjsLine1257" x1="0"
                                                    y1="0" x2="289.6875" y2="0"
                                                    stroke="#b6b6b6" stroke-dasharray="0"
                                                    stroke-width="1" stroke-linecap="butt"
                                                    class="apexcharts-ycrosshairs"></line>
                                                <line id="SvgjsLine1258" x1="0"
                                                    y1="0" x2="289.6875" y2="0"
                                                    stroke-dasharray="0" stroke-width="0"
                                                    stroke-linecap="butt"
                                                    class="apexcharts-ycrosshairs-hidden"></line>
                                                <g id="SvgjsG1259"
                                                    class="apexcharts-yaxis-annotations">
                                                </g>
                                                <g id="SvgjsG1260"
                                                    class="apexcharts-xaxis-annotations">
                                                </g>
                                                <g id="SvgjsG1261"
                                                    class="apexcharts-point-annotations">
                                                </g>
                                                <rect id="SvgjsRect1262" width="0"
                                                    height="0" x="0" y="0"
                                                    rx="0" ry="0" opacity="1"
                                                    stroke-width="0" stroke="none"
                                                    stroke-dasharray="0" fill="#fefefe"
                                                    class="apexcharts-zoom-rect"></rect>
                                                <rect id="SvgjsRect1263" width="0"
                                                    height="0" x="0" y="0"
                                                    rx="0" ry="0" opacity="1"
                                                    stroke-width="0" stroke="none"
                                                    stroke-dasharray="0" fill="#fefefe"
                                                    class="apexcharts-selection-rect"></rect>
                                            </g>
                                            <rect id="SvgjsRect1190" width="0" height="0"
                                                x="0" y="0" rx="0"
                                                ry="0" opacity="1" stroke-width="0"
                                                stroke="none" stroke-dasharray="0"
                                                fill="#fefefe">
                                            </rect>
                                            <g id="SvgjsG1245" class="apexcharts-yaxis"
                                                rel="0" transform="translate(-8, 0)">
                                                <g id="SvgjsG1246" class="apexcharts-yaxis-texts-g">
                                                </g>
                                            </g>
                                            <g id="SvgjsG1188" class="apexcharts-annotations"></g>
                                        </svg>
                                        <div class="apexcharts-legend" style="max-height: 107.5px;">
                                        </div>
                                        <div class="apexcharts-tooltip apexcharts-theme-light">
                                            <div class="apexcharts-tooltip-title"
                                                style="font-family: Helvetica, Arial, sans-serif; font-size: 12px;">
                                            </div>
                                            <div class="apexcharts-tooltip-series-group"
                                                style="order: 1;"><span
                                                    class="apexcharts-tooltip-marker"
                                                    style="background-color: rgb(105, 108, 255);"></span>
                                                <div class="apexcharts-tooltip-text"
                                                    style="font-family: Helvetica, Arial, sans-serif; font-size: 12px;">
                                                    <div class="apexcharts-tooltip-y-group"><span
                                                            class="apexcharts-tooltip-text-y-label"></span><span
                                                            class="apexcharts-tooltip-text-y-value"></span>
                                                    </div>
                                                    <div class="apexcharts-tooltip-goals-group"><span
                                                            class="apexcharts-tooltip-text-goals-label"></span><span
                                                            class="apexcharts-tooltip-text-goals-value"></span>
                                                    </div>
                                                    <div class="apexcharts-tooltip-z-group"><span
                                                            class="apexcharts-tooltip-text-z-label"></span><span
                                                            class="apexcharts-tooltip-text-z-value"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div
                                            class="apexcharts-xaxistooltip apexcharts-xaxistooltip-bottom apexcharts-theme-light">
                                            <div class="apexcharts-xaxistooltip-text"
                                                style="font-family: Helvetica, Arial, sans-serif; font-size: 12px;">
                                            </div>
                                        </div>
                                        <div
                                            class="apexcharts-yaxistooltip apexcharts-yaxistooltip-0 apexcharts-yaxistooltip-left apexcharts-theme-light">
                                            <div class="apexcharts-yaxistooltip-text"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-center pt-4 gap-2">
                                    <div class="flex-shrink-0" style="position: relative;">
                                        <div id="expensesOfWeek" style="min-height: 57.7px;">
                                            <div id="apexchartsr4iubjt3"
                                                class="apexcharts-canvas apexchartsr4iubjt3 apexcharts-theme-light"
                                                style="width: 60px; height: 57.7px;"><svg
                                                    id="SvgjsSvg1264" width="60"
                                                    height="57.7"
                                                    xmlns="http://www.w3.org/2000/svg"
                                                    version="1.1"
                                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                                    xmlns:svgjs="http://svgjs.dev"
                                                    class="apexcharts-svg" xmlns:data="ApexChartsNS"
                                                    transform="translate(0, 0)"
                                                    style="background: transparent;">
                                                    <g id="SvgjsG1266"
                                                        class="apexcharts-inner apexcharts-graphical"
                                                        transform="translate(-10, -10)">
                                                        <defs id="SvgjsDefs1265">
                                                            <clipPath id="gridRectMaskr4iubjt3">
                                                                <rect id="SvgjsRect1268"
                                                                    width="86" height="87"
                                                                    x="-3" y="-1"
                                                                    rx="0" ry="0"
                                                                    opacity="1" stroke-width="0"
                                                                    stroke="none"
                                                                    stroke-dasharray="0"
                                                                    fill="#fff">
                                                                </rect>
                                                            </clipPath>
                                                            <clipPath id="forecastMaskr4iubjt3">
                                                            </clipPath>
                                                            <clipPath id="nonForecastMaskr4iubjt3">
                                                            </clipPath>
                                                            <clipPath id="gridRectMarkerMaskr4iubjt3">
                                                                <rect id="SvgjsRect1269"
                                                                    width="84" height="89"
                                                                    x="-2" y="-2"
                                                                    rx="0" ry="0"
                                                                    opacity="1" stroke-width="0"
                                                                    stroke="none"
                                                                    stroke-dasharray="0"
                                                                    fill="#fff">
                                                                </rect>
                                                            </clipPath>
                                                        </defs>
                                                        <g id="SvgjsG1270"
                                                            class="apexcharts-radialbar">
                                                            <g id="SvgjsG1271">
                                                                <g id="SvgjsG1272"
                                                                    class="apexcharts-tracks">
                                                                    <g id="SvgjsG1273"
                                                                        class="apexcharts-radialbar-track apexcharts-track"
                                                                        rel="1">
                                                                        <path
                                                                            id="apexcharts-radialbarTrack-0"
                                                                            d="M 40 18.098170731707313 A 21.901829268292687 21.901829268292687 0 1 1 39.99617740968999 18.098171065291247"
                                                                            fill="none"
                                                                            fill-opacity="1"
                                                                            stroke="rgba(236,238,241,0.85)"
                                                                            stroke-opacity="1"
                                                                            stroke-linecap="round"
                                                                            stroke-width="2.0408536585365864"
                                                                            stroke-dasharray="0"
                                                                            class="apexcharts-radialbar-area"
                                                                            data:pathOrig="M 40 18.098170731707313 A 21.901829268292687 21.901829268292687 0 1 1 39.99617740968999 18.098171065291247">
                                                                        </path>
                                                                    </g>
                                                                </g>
                                                                <g id="SvgjsG1275">
                                                                    <g id="SvgjsG1279"
                                                                        class="apexcharts-series apexcharts-radial-series"
                                                                        seriesName="seriesx1"
                                                                        rel="1"
                                                                        data:realIndex="0">
                                                                        <path id="SvgjsPath1280"
                                                                            d="M 40 18.098170731707313 A 21.901829268292687 21.901829268292687 0 1 1 22.2810479140526 52.873572242130095"
                                                                            fill="none"
                                                                            fill-opacity="0.85"
                                                                            stroke="rgba(105,108,255,0.85)"
                                                                            stroke-opacity="1"
                                                                            stroke-linecap="round"
                                                                            stroke-width="4.081707317073173"
                                                                            stroke-dasharray="0"
                                                                            class="apexcharts-radialbar-area apexcharts-radialbar-slice-0"
                                                                            data:angle="234"
                                                                            data:value="65"
                                                                            index="0"
                                                                            j="0"
                                                                            data:pathOrig="M 40 18.098170731707313 A 21.901829268292687 21.901829268292687 0 1 1 22.2810479140526 52.873572242130095">
                                                                        </path>
                                                                    </g>
                                                                    <circle id="SvgjsCircle1276"
                                                                        r="18.881402439024395"
                                                                        cx="40"
                                                                        cy="40"
                                                                        class="apexcharts-radialbar-hollow"
                                                                        fill="transparent"></circle>
                                                                    <g id="SvgjsG1277"
                                                                        class="apexcharts-datalabels-group"
                                                                        transform="translate(0, 0) scale(1)"
                                                                        style="opacity: 1;"><text
                                                                            id="SvgjsText1278"
                                                                            font-family="Helvetica, Arial, sans-serif"
                                                                            x="40"
                                                                            y="45"
                                                                            text-anchor="middle"
                                                                            dominant-baseline="auto"
                                                                            font-size="13px"
                                                                            font-weight="400"
                                                                            fill="#697a8d"
                                                                            class="apexcharts-text apexcharts-datalabel-value"
                                                                            style="font-family: Helvetica, Arial, sans-serif;">$65</text>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                        <line id="SvgjsLine1281" x1="0"
                                                            y1="0" x2="80"
                                                            y2="0" stroke="#b6b6b6"
                                                            stroke-dasharray="0" stroke-width="1"
                                                            stroke-linecap="butt"
                                                            class="apexcharts-ycrosshairs"></line>
                                                        <line id="SvgjsLine1282" x1="0"
                                                            y1="0" x2="80"
                                                            y2="0" stroke-dasharray="0"
                                                            stroke-width="0" stroke-linecap="butt"
                                                            class="apexcharts-ycrosshairs-hidden">
                                                        </line>
                                                    </g>
                                                    <g id="SvgjsG1267"
                                                        class="apexcharts-annotations">
                                                    </g>
                                                </svg>
                                                <div class="apexcharts-legend"></div>
                                            </div>
                                        </div>
                                        <div class="resize-triggers">
                                            <div class="expand-trigger">
                                                <div style="width: 61px; height: 59px;"></div>
                                            </div>
                                            <div class="contract-trigger"></div>
                                        </div>
                                    </div>
                                    <div>
                                        <p class="mb-n1 mt-1">Expenses This Week</p>
                                        <small class="text-muted">$39 less than last week</small>
                                    </div>
                                </div>
                                <div class="resize-triggers">
                                    <div class="expand-trigger">
                                        <div style="width: 308px; height: 377px;"></div>
                                    </div>
                                    <div class="contract-trigger"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ Expense Overview -->

            <!-- Transactions -->
            <div class="col-md-6 col-lg-4 order-2 mb-4">
                <div class="card h-100">
                    <div class="card-header d-flex align-items-center justify-content-between">
                        <h5 class="card-title m-0 me-2">Transactions</h5>
                        <div class="dropdown">
                            <button class="btn p-0" type="button" id="transactionID"
                                data-bs-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                                <i class="bx bx-dots-vertical-rounded"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-end"
                                aria-labelledby="transactionID">
                                <a class="dropdown-item" href="javascript:void(0);">Last 28 Days</a>
                                <a class="dropdown-item" href="javascript:void(0);">Last Month</a>
                                <a class="dropdown-item" href="javascript:void(0);">Last Year</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <ul class="p-0 m-0">
                            <li class="d-flex mb-4 pb-1">
                                <div class="avatar flex-shrink-0 me-3">
                                    <img src="../assets/img/icons/unicons/paypal.png" alt="User"
                                        class="rounded">
                                </div>
                                <div
                                    class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                                    <div class="me-2">
                                        <small class="text-muted d-block mb-1">Paypal</small>
                                        <h6 class="mb-0">Send money</h6>
                                    </div>
                                    <div class="user-progress d-flex align-items-center gap-1">
                                        <h6 class="mb-0">+82.6</h6>
                                        <span class="text-muted">USD</span>
                                    </div>
                                </div>
                            </li>
                            <li class="d-flex mb-4 pb-1">
                                <div class="avatar flex-shrink-0 me-3">
                                    <img src="../assets/img/icons/unicons/wallet.png" alt="User"
                                        class="rounded">
                                </div>
                                <div
                                    class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                                    <div class="me-2">
                                        <small class="text-muted d-block mb-1">Wallet</small>
                                        <h6 class="mb-0">Mac'D</h6>
                                    </div>
                                    <div class="user-progress d-flex align-items-center gap-1">
                                        <h6 class="mb-0">+270.69</h6>
                                        <span class="text-muted">USD</span>
                                    </div>
                                </div>
                            </li>
                            <li class="d-flex mb-4 pb-1">
                                <div class="avatar flex-shrink-0 me-3">
                                    <img src="../assets/img/icons/unicons/chart.png" alt="User"
                                        class="rounded">
                                </div>
                                <div
                                    class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                                    <div class="me-2">
                                        <small class="text-muted d-block mb-1">Transfer</small>
                                        <h6 class="mb-0">Refund</h6>
                                    </div>
                                    <div class="user-progress d-flex align-items-center gap-1">
                                        <h6 class="mb-0">+637.91</h6>
                                        <span class="text-muted">USD</span>
                                    </div>
                                </div>
                            </li>
                            <li class="d-flex mb-4 pb-1">
                                <div class="avatar flex-shrink-0 me-3">
                                    <img src="../assets/img/icons/unicons/cc-success.png"
                                        alt="User" class="rounded">
                                </div>
                                <div
                                    class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                                    <div class="me-2">
                                        <small class="text-muted d-block mb-1">Credit Card</small>
                                        <h6 class="mb-0">Ordered Food</h6>
                                    </div>
                                    <div class="user-progress d-flex align-items-center gap-1">
                                        <h6 class="mb-0">-838.71</h6>
                                        <span class="text-muted">USD</span>
                                    </div>
                                </div>
                            </li>
                            <li class="d-flex mb-4 pb-1">
                                <div class="avatar flex-shrink-0 me-3">
                                    <img src="../assets/img/icons/unicons/wallet.png" alt="User"
                                        class="rounded">
                                </div>
                                <div
                                    class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                                    <div class="me-2">
                                        <small class="text-muted d-block mb-1">Wallet</small>
                                        <h6 class="mb-0">Starbucks</h6>
                                    </div>
                                    <div class="user-progress d-flex align-items-center gap-1">
                                        <h6 class="mb-0">+203.33</h6>
                                        <span class="text-muted">USD</span>
                                    </div>
                                </div>
                            </li>
                            <li class="d-flex">
                                <div class="avatar flex-shrink-0 me-3">
                                    <img src="../assets/img/icons/unicons/cc-warning.png"
                                        alt="User" class="rounded">
                                </div>
                                <div
                                    class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                                    <div class="me-2">
                                        <small class="text-muted d-block mb-1">Mastercard</small>
                                        <h6 class="mb-0">Ordered Food</h6>
                                    </div>
                                    <div class="user-progress d-flex align-items-center gap-1">
                                        <h6 class="mb-0">-92.45</h6>
                                        <span class="text-muted">USD</span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--/ Transactions -->
        </div>
    </div>
    <!-- / Content -->

@endsection
