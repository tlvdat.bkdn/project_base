const profile = (() => {
    let method_profile = {};

    method_profile.initSelect2 = (element,url ) => {
        $(element).select2({
            ajax: {
                url: url,
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function (data) {
                    let term = this.container.results.lastParams.term;
                    if (term) data = data.filter(d => d.name.includes(term));
                    return {
                        results: $.map(data, function (item) {
                            return {
                                id: item.id,
                                text: helper.method_helper.capitalizeFirstLetter(item.name)
                            };
                        })
                    };
                }
            },
            selectionCssClass: "custom-select"
        });
    }

    method_profile.setDefaultSelect = (element) => {
        if (DATA_COUTRY[element]){
            var newOption = new Option(DATA_COUTRY[element].name, DATA_COUTRY[element].id, true, true);
            $(`#${element}_id`).append(newOption).trigger('change');
        }
    }

    method_profile.clearSelect = (element) => {
        $('#' + element).val('');
        $(`#${element}`).trigger('change');
    }

    return {
        method_profile
    }
})();

$(function () {
    profile.method_profile.initSelect2("#country_id", URL_API_COUNTRY);
    profile.method_profile.initSelect2("#state_id", URL_API_STATE);
    profile.method_profile.initSelect2("#city_id", URL_API_CITY);
    profile.method_profile.setDefaultSelect("country");
    profile.method_profile.setDefaultSelect("state");
    profile.method_profile.setDefaultSelect("city");

    $("body").on('change', "#country_id", function () {
        profile.method_profile.clearSelect('state_id')
        profile.method_profile.clearSelect('city_id')
        profile.method_profile.initSelect2("#state_id", URL_API_STATE + `?country_id=${$('select[name="country_id"]').val()}`);
    })

    $("body").on('change', "#state_id", function () {
        profile.method_profile.clearSelect('city_id')
        profile.method_profile.initSelect2("#city_id", URL_API_CITY + `?state_id=${$('select[name="state_id"]').val()}`);
    })
});
