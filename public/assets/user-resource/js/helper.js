const helper = (() => {
    let method_helper = {};

    method_helper.setToast = (heading, msg, icon) => {
        $.toast({
            heading: heading,
            text: msg,
            icon: icon,
            loader: true, // Change it to false to disable loader
            position: 'bottom-right',
        })
    }
    method_helper.capitalizeFirstLetter = (string) => {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    return {
        method_helper
    }
})();
